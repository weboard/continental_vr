// Fill out your copyright notice in the Description page of Project Settings.

#include "FileDownloader.h"

#include "Misc/Paths.h"
#include "HAL/PlatformFilemanager.h"
#include "PromethistManager.h"

uint32 UFileDownloader::AudioFilesCount = 1;

UFileDownloader::UFileDownloader() :
	FileUrl(TEXT(""))
	, FileSavePath(TEXT(""))
{
}

UFileDownloader::~UFileDownloader()
{
}

UFileDownloader* UFileDownloader::CreateFileDownloader()
{
	UFileDownloader* Downloader = NewObject<UFileDownloader>();
	return Downloader;
}

UFileDownloader* UFileDownloader::DownloadFile(const FString& Url)
{
	FileUrl = Url;

	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> HttpRequest = FHttpModule::Get().CreateRequest();
	HttpRequest->SetVerb("GET");
	HttpRequest->SetURL(Url);
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &UFileDownloader::OnReady);
	HttpRequest->OnRequestProgress().BindUObject(this, &UFileDownloader::OnProgress_Internal);

	// Execute the request
	HttpRequest->ProcessRequest();
	AddToRoot();

	return this;
}

void UFileDownloader::OnReady(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	RemoveFromRoot();
	Request->OnProcessRequestComplete().Unbind();	

	if (Response.IsValid() && EHttpResponseCodes::IsOk(Response->GetResponseCode()))
	{
		// SAVE FILE
		IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

		FString Path, Filename, Extension;
		FPaths::Split(FileUrl, Path, Filename, Extension);
		
		FileSavePath = FPaths::ProjectContentDir() + "ProjectAila/DownloadedSounds/sound" + FString::FromInt(UFileDownloader::AudioFilesCount) + ".mp3";
		
		UE_LOG(LogTemp, Log, TEXT("=============== Downloading file from: %s"), *FileUrl);
		UE_LOG(LogTemp, Log, TEXT("=============== Downloading file to: %s"), *FileSavePath);
		
		// create save directory if not existent
		FPaths::Split(FileSavePath, Path, Filename, Extension);
		if (!PlatformFile.DirectoryExists(*Path))
		{
			if (!PlatformFile.CreateDirectoryTree(*Path))
			{
				OnResult.Broadcast(EDownloadResult::DirectoryCreationFailed);
				return;
			}
		}

		// open/create the file
		IFileHandle* FileHandle = PlatformFile.OpenWrite(*FileSavePath);
		if (FileHandle)
		{
			// write the file
			FileHandle->Write(Response->GetContent().GetData(), Response->GetContentLength());
			// Close the file
			delete FileHandle;

			OnResult.Broadcast(EDownloadResult::Success);
			Manager->DownloadedFiles.Enqueue(FileSavePath);
			Manager->ConvertSound(FileSavePath);
			Manager->DownloadFile(); // Download next file
			UFileDownloader::AudioFilesCount += 1;
		}
		else
		{
			OnResult.Broadcast(EDownloadResult::SaveFailed);
		}
	}
	else
	{
		OnResult.Broadcast(EDownloadResult::DownloadFailed);
	}
}

void UFileDownloader::OnProgress_Internal(FHttpRequestPtr Request, int32 BytesSent, int32 BytesReceived)
{
	int32 FullSize = Request->GetContentLength();
	OnProgress.Broadcast(BytesSent, BytesReceived, FullSize);
}