#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Containers/Queue.h"
#include "PromethistManager.generated.h"

class UPromethistAudioManager;
class UPromethistWS;
class UFileDownloader;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAudioInputReadyEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPromethistConnectedEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCommandReceivedEvent, FString, Command);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnAudioSampleReadyEvent, USoundWave*, Sound, const TArray<uint8>&, RawData, FString, Voice, FString, Command);


/**
 * This is just a wrapper class for all the Promethist functionality to be in one place.
 */
UCLASS(BlueprintType)
class SAFETYVR_API UPromethistManager : public UObject
{
	GENERATED_BODY()

public:
	
	// Default Constructor
	UPromethistManager();

	// Static class for creating Promethist Manager UObject reference
	UFUNCTION(BlueprintCallable, Category = "Promethist|Manager")
		static UPromethistManager* CreatePromethistManager();
	
	// Initialize connection to the provided WebSocket
	UFUNCTION(BlueprintCallable, Category = "Promethist|Manager")
		void InitConnection(FString Url = FString("wss://port.promethist.com/socket/"), FString Protocol = FString("wss"));
	

	// Event called when WebSocket is connected to the server
	UPROPERTY(EditAnywhere, BlueprintReadWrite, BlueprintAssignable, Category = "Promethist|Manager")
		FPromethistConnectedEvent OnConnectedEvent;

	/**
	* Bind to know when the server is ready to listen to user audio input
	*/
	UPROPERTY(BlueprintAssignable, Category = "Promethist|Manager")
		FAudioInputReadyEvent OnAudioInputReady;

	/**
	* Bind to know when the downloaded and converted audio sample from Promethist is ready to be sent to Oculus Audio PCM
	*/
	UPROPERTY(BlueprintAssignable, Category = "Promethist|Manager")
		FOnAudioSampleReadyEvent OnAudioSampleReady;

	/**
	* Bind to know when Promethist sends us command
	*/
	UPROPERTY(BlueprintAssignable, Category = "Promethist|Manager")
		FOnCommandReceivedEvent OnCommandReceivedEvent;
	
	/**
	 * Initialize conversation (Event = Init)
	 * 
	 * @param AppKey Application key ID to be used for conversation (can be Application or Dialogue ID)
	 * @param SilenceTimeout Time in milliseconds until #Silence action is called on Promethist (delay between silence and speech)
	 */
	UFUNCTION(BlueprintCallable, Category = "Promethist|Manager")
		void InitConversation(const FString AppKey = FString("5f744a7cd179a105d0c3f00c"), int SilenceTimeout = 5000);

	/**
	* Starts downloading a file and saves it when done. Bind to the OnResult
	* event to know when the download is done.
	* File is saved to the "ProjectAila/DownloadedSounds/" project folder.
	*/
	UFUNCTION(BlueprintCallable, Category = "Promethist|Manager")
		void DownloadFile();


	/**
	 * Helper function for setting the world context (used in timers)
	 */
	UFUNCTION(BlueprintCallable, Category = "Promethist|Manager", meta = (WorldContext = "WorldContextObject"))
		void SetWorld(UObject* WorldContextObject);


	/*
	 * Converts mp3 to UE4 sound format
	 *
	 * @param Url	Path to the sound
	 */
	UFUNCTION()
	void ConvertSound(FString Url);

	/*
	 * Sends audio binary data to the WebSocket
	 *
	 * @param BinaryAudio	Audio in binary format from microphone
	 * @param Silence	Are we sending just silence (zeroed bytes)
	 */
	UFUNCTION()
		void SendAudio(TArray<uint8> BinaryAudio, bool Silence);

	/*
	 * Starts recording microphone input
	 */
	UFUNCTION(BlueprintCallable)
		void StartRecordingAudio();

	/*
	 * Stops recording microphone input
	 */
	UFUNCTION(BlueprintCallable)
		void StopRecordingAudio();

	/*
	 * Enables the microphone voice capture input
	 */
	UFUNCTION(BlueprintCallable)
		void EnableVoiceCapture();

	/**
	 * Sends user event to the WebSocket
	 */
	UFUNCTION(BlueprintCallable, Category = "Promethist|WebSocket")
		void SendEventToServer(FString Event);
	
	/*
	 * Sets the current WebSocket's connection status
	 *
	 * @param Status	Connection status. True = connected.
	 */
	void SetConnected(bool Status);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPromethistWS* WebSocket;

	// Child private managers, created on init
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPromethistAudioManager* AudioManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UFileDownloader* FileDownloader;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UWorld* WorldObject;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bRecordingEnabled = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bInputEnabled = true;
	
	uint32 CurrentAudioCount;
	uint32 TotalAudioCount;
	
	TQueue<FString> FilesToDownload;
	TQueue<FString> DownloadedFiles;
	TQueue<FString> Voices;
	FString Command;
	

private:

	// Checks
	bool Connected;
	
};
