// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "json.hpp"
#include "PromethistWS.generated.h"

// Forward declarations
class UBlueprintWebSocket;
class UPromethistManager;

UENUM()
enum EServerEventType
{
    Ready       UMETA(DisplayName = "Ready"),
    Response       UMETA(DisplayName = "Response"),
    Recognized       UMETA(DisplayName = "Recognized"),
    SessionStarted       UMETA(DisplayName = "SessionStarted"),
    SessionEnded       UMETA(DisplayName = "SessionEnded"),
    InputAudioStreamOpen       UMETA(DisplayName = "SessionEnded"),
    InputAudioStreamClose       UMETA(DisplayName = "SessionEnded")
};

/**
 * 
 */
UCLASS()
class SAFETYVR_API UPromethistWS : public UObject
{
	GENERATED_BODY()

public:

	/**
	 * Initializes connection to the WebSocket server.
	 * This function is managed from the UPromethistManager, should not be called on it's own.
	 *
	 * @param Url       WebSocket URL address
	 * @patam Protocol  WebSocket protocol (ws/wss)
	 */
    UFUNCTION()
    void InitializeAndConnectSocket(FString Url, FString Protocol);

    /**
     * Initialize conversation (Event = Init)
     * This function is managed from the UPromethistManager, should not be called on it's own.
     *
     * @param AppKey Application key ID to be used for conversation (can be Application or Dialogue ID)
     * @param SilenceTimeout Time in milliseconds until #Silence action is called on Promethist (delay between silence and speech)
     */
    UFUNCTION(Category = "Promethist|WebSocket")
        void InitConversation(FString AppKey, int SilenceTimeout);

	// Constructor
    UPromethistWS();

    /**
     * Construct this object
     *
     * @param Manager   Owner of this object (should be UPromethistManager)
     */
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Promethist|WebSocket")
		static UPromethistWS* CreatePromethistWS(UPromethistManager* Manager);

	/**
	 * Send Message to the current WebSocket server
	 *
	 * @param Message   Message to be sent in JSON string format
	 */
    UFUNCTION(BlueprintCallable, Category = "Promethist|WebSocket")
        void SendMessage(FString Message);

    /**
     * Send Binary Message to the current WebSocket server
     *
     * @param BinaryMessage   Binary Message to be sent in binary array uint8 format
     */
    UFUNCTION(BlueprintCallable, Category = "Promethist|WebSocket")
        void SendMessageBinary(TArray<uint8> BinaryMessage, bool Silence);

	/**
	 * Tells server to start listening for a audio input
	 */
    UFUNCTION(BlueprintCallable, Category = "Promethist|WebSocket")
        void EnableAudioInput();

    /**
     * Tells server to end listening for a audio input
     */
    UFUNCTION(BlueprintCallable, Category = "Promethist|WebSocket")
        void DisableAudioInput();

    /**
     * Sends user event to the WebSocket
     */
    UFUNCTION(BlueprintCallable, Category = "Promethist|WebSocket")
		void SendEventToServer(FString Event);

	/**
	 * Get a reference to the WebSocket UObject
	 */
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Promethist|WebSocket")
        UBlueprintWebSocket* GetWebSocket();


    // Manager of this WebSocket
	UPROPERTY()
    UPromethistManager* Manager;

    UPROPERTY()
        TArray<uint8> AudioToSend;
	
private:
    // Callbacks
    UFUNCTION() void OnConnected();
    UFUNCTION() void OnConnectionError(const FString& Error);
    UFUNCTION() void OnClosed(int64 StatusCode, const FString& Reason, bool bWasClean);
    UFUNCTION() void OnMessage(const FString& Message);
    UFUNCTION() void OnRawMessage(const TArray<uint8>& Data, int32 BytesRemaining);
    UFUNCTION() void OnMessageSent(const FString& Message);

	// Websocket reference
    UPROPERTY(VisibleAnywhere, Category = "Promethist|WebSocket")
        UBlueprintWebSocket* WebSocket;
	
    // AppKey to be used for conversation
    UPROPERTY(VisibleAnywhere, Category = "Promethist|WebSocket")
        FString AppKeyWS;

    bool bAudioEnabled = false;
    bool bSessionEnded = false;

    std::string UUID;

	// Wakes server from Sleeping state (start conversation)
    void PokeServer();

    // Parses message from WebSocket in type of "response"
    void ParseResponse(nlohmann::json j);

};
