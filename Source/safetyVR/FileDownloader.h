#pragma once

#include "CoreMinimal.h"
#include "Http.h"
#include "UObject/NoExportTypes.h"
#include "FileDownloader.generated.h"

class UPromethistManager;

/**
* Possible results from a download request.
*/
UENUM(BlueprintType, Category = "HTTP")
enum class EDownloadResult : uint8
{
	Success,
	DownloadFailed,
	SaveFailed,
	DirectoryCreationFailed
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnResult, const EDownloadResult, Result);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnProgress, const int32, BytesSent, const int32, BytesReceived, const int32, ContentLength);

/**
 * 
 */
UCLASS()
class SAFETYVR_API UFileDownloader : public UObject
{
	GENERATED_BODY()

public:
	/**
	* Bind to know when the download is complete (even if it fails).
	*/
	UPROPERTY(BlueprintAssignable, Category = "HTTP")
		FOnResult OnResult;
	/**
	* Bind to know when the download is complete (even if it fails).
	*/
	UPROPERTY(BlueprintAssignable, Category = "HTTP")
		FOnProgress OnProgress;

	/**
	* The URL used to start this download.
	*/
	UPROPERTY(BlueprintReadOnly, Category = "HTTP")
		FString FileUrl;
	/**
	* The path set to save the downloaded file.
	*/
	UPROPERTY(BlueprintReadOnly, Category = "HTTP")
		FString FileSavePath;

	// Constructors
	UFileDownloader();
	~UFileDownloader();

	/**
	* Instantiates a FileDownloader object, starts downloading and saves it when done.
	*
	* @return The FileDownloader object. Bind to it's OnResult event to know when it's done downloading.
	*/
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HTTP")
		static UFileDownloader* CreateFileDownloader();

	/**
	* Starts downloading a file and saves it when done. Bind to the OnResult
	* event to know when the download is done.
	* File is saved to the "ProjectAila/DownloadedSounds/" project folder.
	*
	* @param Url		The file Url to be downloaded.
	* @return Returns itself.
	*/
	UFUNCTION(BlueprintCallable, Category = "HTTP")
		UFileDownloader* DownloadFile(const FString& Url);

	// Manager of this WebSocket
	UPROPERTY()
	UPromethistManager* Manager;

	// Audio files count (shared across all instances)
	static uint32 AudioFilesCount;

private:

	// Callbacks
	void OnReady(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnProgress_Internal(FHttpRequestPtr Request, int32 BytesSent, int32 BytesReceived);

	
	
};
