// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Containers/Queue.h"
#include "PromethistAudioManager.generated.h"

// Forward declarations
class USoundCue;
class USoundWave;
class UPromethistManager;
class FTickableGameObject;
class UAudioComponent;

/**
 * 
 */
UCLASS()
class SAFETYVR_API UPromethistAudioManager : public UObject, public FTickableGameObject
{
	GENERATED_BODY()

public:

	// Constructor
	UPromethistAudioManager();

	/**
	 * Construct this object
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Promethist|Audio")
		static UPromethistAudioManager* CreatePromethistAudioManager();

	/**
	 * Loads and converts audio file (*.mp3)
	 * Uses encode/decode part of a UE4 Plugin "AudioImporter"
	 *
	 * @param Url		URL to the audio file (probably in the "ProjectAila/DownloadedAudio/ folder")
	 * @return			SoundWave UE4 object reference of the converted sound file.
	 */
	UFUNCTION(BlueprintCallable)
		USoundWave* LoadAndConvertAudio(FString Url);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
		void PlaySounds();

	UFUNCTION(BlueprintCallable)
		void StartRecordingAudio();

	UFUNCTION(BlueprintCallable)
		void StopRecordingAudio();


	// Manager of this WebSocket
	UPROPERTY()
		UPromethistManager* Manager;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Promethist|Audio")
		float DelayBetweenSounds;
		

	void Tick(float DeltaTime) override;
	bool IsTickable() const override;
	bool IsTickableInEditor() const override;
	bool IsTickableWhenPaused() const override;
	TStatId GetStatId() const override;
	UWorld* GetWorld() const override;

	TQueue<USoundWave*> ConvertedSounds;
	TQueue<TArray<uint8>> ConvertedSoundsRaw;
	TSharedPtr<class IVoiceCapture> VoiceCapture;

	UPROPERTY()
		TArray<uint8> VoiceCaptureBuffer; //voice data is saved here

private:

	bool bCanSendSilence = false;

};