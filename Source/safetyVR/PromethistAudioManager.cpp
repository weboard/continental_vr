// Fill out your copyright notice in the Description page of Project Settings.


#include "PromethistAudioManager.h"
#include "AudioImporterRuntime.h"
#include "Engine/Engine.h"
#include "Sound/SoundWave.h"
#include "PromethistManager.h"
#include "Kismet/GameplayStatics.h"
#include "Tickable.h"
#include "Voice/Public/VoiceModule.h"
#include "Voice/Public/Voice.h"

UPromethistAudioManager* UPromethistAudioManager::CreatePromethistAudioManager()
{
    UE_LOG(LogTemp, Warning, TEXT("=============== Creating Audio Manager"));
    return NewObject<UPromethistAudioManager>();
}

UPromethistAudioManager::UPromethistAudioManager() :Super()
{
    VoiceCapture = FVoiceModule::Get().CreateVoiceCapture("", 16000, 1);
    UVOIPStatics::SetMicThreshold(0.0); // Mic is now always on!
    DelayBetweenSounds = 1.0f;
}

USoundWave* UPromethistAudioManager::LoadAndConvertAudio(FString Url)
{
    TArray<uint8> RawData;
    USoundWave* Sound = UAudioImporterRuntime::ConvertAudioFile(Manager->WorldObject, Url, RawData);
    Sound->AddToRoot();
    ConvertedSounds.Enqueue(Sound);
    ConvertedSoundsRaw.Enqueue(RawData);
    return Sound;
}

void UPromethistAudioManager::PlaySounds()
{	
    if (!ConvertedSounds.IsEmpty())
    {
        USoundWave* Sound;
        TArray<uint8> RawData;
        FString Voice;
        FString Command;
        ConvertedSounds.Dequeue(Sound);
        ConvertedSoundsRaw.Dequeue(RawData);
        Manager->Voices.Dequeue(Voice);

    	if (!Manager->Command.IsEmpty())
    	{
            Command = Manager->Command;
            Manager->Command.Empty();
    	}
    	
        UE_LOG(LogTemp, Warning, TEXT("=============== Sending sound to Oculus PCM: %s"), *Sound->GetName());
        
    	
    	if (IsValid(Sound))
    	{
            Manager->OnAudioSampleReady.Broadcast(Sound, RawData, Voice, Command);
    	}

        FTimerHandle AudioDurationHandle;        

    	if (ConvertedSounds.IsEmpty())
    	{
            Manager->WorldObject->GetTimerManager().SetTimer(AudioDurationHandle, this, &UPromethistAudioManager::PlaySounds, Sound->GetDuration() + 0.5f, false);
    	}
    	else
    	{
            Manager->WorldObject->GetTimerManager().SetTimer(AudioDurationHandle, this, &UPromethistAudioManager::PlaySounds, Sound->GetDuration() + DelayBetweenSounds, false);
    	}
    }
	else
    {
        // Played all sounds, waiting for user mic input
        
		if (Manager->bInputEnabled) // Enable input only without #noinput command from server
		{
            Manager->StartRecordingAudio();
            UE_LOG(LogTemp, Warning, TEXT("=============== Played all sounds, starting mic input!"));
		} else
		{
            UE_LOG(LogTemp, Warning, TEXT("=============== Played all sounds, mic input disabled for this speech!"));
		}
    }
}

void UPromethistAudioManager::StartRecordingAudio()
{
    UE_LOG(LogTemp, Warning, TEXT("Started recording"));
	VoiceCapture->Start();
}

void UPromethistAudioManager::Tick(float DeltaTime)
{
    if (VoiceCapture.IsValid() && VoiceCapture->IsCapturing())
    {
    	
        VoiceCaptureBuffer.Reset();
        uint32 VoiceCaptureBytesAvailable = 0;
        EVoiceCaptureState::Type CaptureState = VoiceCapture->GetCaptureState(VoiceCaptureBytesAvailable);
        uint32 VoiceCaptureReadBytes;       

        //GEngine->AddOnScreenDebugMessage(0, 0, FColor::Green, FString("Microphone ON"));
    	
        if (CaptureState == EVoiceCaptureState::Ok && VoiceCaptureBytesAvailable > 0)
        {
            VoiceCaptureBuffer.SetNumUninitialized(VoiceCaptureBytesAvailable);
            VoiceCapture->GetVoiceData(VoiceCaptureBuffer.GetData(), VoiceCaptureBytesAvailable, VoiceCaptureReadBytes);
            Manager->SendAudio(VoiceCaptureBuffer, false);
        }
    	
    }
    else
    {
        //GEngine->AddOnScreenDebugMessage(0, 0, FColor::Green, FString("Microphone OFF"));
    }
}

void UPromethistAudioManager::StopRecordingAudio()
{
    if (VoiceCapture.Get() != NULL)
    {
        VoiceCaptureBuffer.Reset();
        VoiceCapture->Stop();
        UE_LOG(LogTemp, Warning, TEXT("Stopped Recording"));
    }

    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Microphone Not Plugged In"));
    }
}

bool UPromethistAudioManager::IsTickable() const
{
	return true;
}

bool UPromethistAudioManager::IsTickableInEditor() const
{
	return false;
}

bool UPromethistAudioManager::IsTickableWhenPaused() const
{
	return false;
}

TStatId UPromethistAudioManager::GetStatId() const
{
	return TStatId();
}

UWorld* UPromethistAudioManager::GetWorld() const
{
	return Manager->WorldObject;
}