#include "PromethistManager.h"
#include "PromethistWS.h"
#include "PromethistAudioManager.h"
#include "FileDownloader.h"
#include "json.hpp"

using json = nlohmann::json;

UPromethistManager* UPromethistManager::CreatePromethistManager()
{
    UE_LOG(LogTemp, Warning, TEXT("=============== Creating Promethist Manager"));  
    return NewObject<UPromethistManager>();
}

UPromethistManager::UPromethistManager()
{
    UE_LOG(LogTemp, Warning, TEXT("=============== Creating Managers"));

    // Creating other managers
    WebSocket = CreateDefaultSubobject<UPromethistWS>(TEXT("WebSocket"));
    WebSocket->Manager = this;

    AudioManager = CreateDefaultSubobject<UPromethistAudioManager>(TEXT("AudioManager"));
    AudioManager->Manager = this;

    FileDownloader = CreateDefaultSubobject<UFileDownloader>(TEXT("FileDownloader"));
    FileDownloader->Manager = this;

    CurrentAudioCount = 0;
    TotalAudioCount = 0;
    WorldObject = nullptr;
}

void UPromethistManager::InitConnection(FString Url, FString Protocol)
{
    WebSocket->InitializeAndConnectSocket(Url, Protocol);
}

void UPromethistManager::InitConversation(FString AppKey, int SilenceTimeout)
{
    WebSocket->InitConversation(AppKey, SilenceTimeout);
}

void UPromethistManager::DownloadFile()
{
	if (!FilesToDownload.IsEmpty())
	{
        FString Url;
        FilesToDownload.Dequeue(Url);
        FileDownloader->DownloadFile(Url);
	}
    else
    {
        AudioManager->PlaySounds();
    }    
}

void UPromethistManager::SetWorld(UObject* WorldContextObject)
{
    WorldObject = WorldContextObject->GetWorld();
}

void UPromethistManager::ConvertSound(FString Url)
{
    AudioManager->LoadAndConvertAudio(Url);
}

void UPromethistManager::SendAudio(TArray<uint8> BinaryAudio, bool Silence)
{
    WebSocket->SendMessageBinary(BinaryAudio, Silence);
}

void UPromethistManager::StartRecordingAudio()
{
    WebSocket->EnableAudioInput();
}

void UPromethistManager::StopRecordingAudio()
{
    AudioManager->StopRecordingAudio();
}

void UPromethistManager::EnableVoiceCapture()
{
    AudioManager->StartRecordingAudio();
    OnAudioInputReady.Broadcast();
}

void UPromethistManager::SendEventToServer(FString Event)
{
    WebSocket->SendEventToServer(Event);
}

void UPromethistManager::SetConnected(bool Status)
{
    Connected = Status;
}
