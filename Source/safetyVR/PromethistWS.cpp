// Fill out your copyright notice in the Description page of Project Settings.


#include "PromethistWS.h"

#include "BlueprintWebSocketWrapper.h"

#include "json.hpp"
#include "PromethistManager.h"
#include "FileDownloader.h"
#include "uuid.hpp"
#include "Kismet/GameplayStatics.h"

using json = nlohmann::json;

// Static class for creating PromethistWS UObject reference
UPromethistWS* UPromethistWS::CreatePromethistWS(UPromethistManager* Manager)
{
    UE_LOG(LogTemp, Warning, TEXT("=============== Creating WebSocket"));
    return NewObject<UPromethistWS>(Manager);
}

// Constructors
UPromethistWS::UPromethistWS()
{
}

void UPromethistWS::InitializeAndConnectSocket(FString Url, FString Protocol)
{
    UE_LOG(LogTemp, Warning, TEXT("=============== Connecting to Promethist WebSocket"));
	
    // Create our new BlueprintWebsocket object.
    WebSocket = UBlueprintWebSocket::CreateWebSocket();

    // Bind the events so our functions are called when the events are triggered.
    WebSocket->OnConnectedEvent.AddDynamic(this, &UPromethistWS::OnConnected);
    WebSocket->OnConnectionErrorEvent.AddDynamic(this, &UPromethistWS::OnConnectionError);
    WebSocket->OnCloseEvent.AddDynamic(this, &UPromethistWS::OnClosed);
    WebSocket->OnMessageEvent.AddDynamic(this, &UPromethistWS::OnMessage);
    WebSocket->OnRawMessageEvent.AddDynamic(this, &UPromethistWS::OnRawMessage);
    WebSocket->OnMessageSentEvent.AddDynamic(this, &UPromethistWS::OnMessageSent);

    // Add our headers.
    //WebSocket->AddHeader(TEXT("SomeHeader"), TEXT("SomeValue"));

    // And we connect.
    WebSocket->Connect(Url, Protocol);
}

void UPromethistWS::OnConnected()
{
    // We successfully connected.
    // It's safe here to send some data to our server.
    UE_LOG(LogTemp, Warning, TEXT("=============== We are connected!"));
    if(Manager)
    {
        Manager->SetConnected(true);
        Manager->OnConnectedEvent.Broadcast();
    }
}

void UPromethistWS::OnConnectionError(const FString& Error)
{
    // Connection failed.
    UE_LOG(LogTemp, Error, TEXT("=============== Failed to connect: %s."), *Error);
    if (Manager)
    {
        Manager->SetConnected(false);
    }
}

void UPromethistWS::OnClosed(int64 StatusCode, const FString& Reason, bool bWasClean)
{
    // The connection has been closed.
    UE_LOG(LogTemp, Warning, TEXT("=============== Connection closed: %d:%s. Clean: %d"), StatusCode, *Reason, bWasClean);
    if (Manager)
    {
        Manager->SetConnected(false);
    }
}

void UPromethistWS::OnMessage(const FString& Message)
{
    // The server sent us a message
    UE_LOG(LogTemp, Warning, TEXT("=============== New message from server: %s"), *Message);

    json j = json::parse(TCHAR_TO_UTF8(*Message));

    if (j.contains("type"))
    {
        FString Type = j["type"].get<std::string>().c_str();
        const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EServerEventType"), true);

    	// Cannot use "switch" on strings in c++
    	// Type: Ready
    	if (Type == EnumPtr->GetNameStringByIndex(EServerEventType::Ready))
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Type: %s"), *Type);
            UE_LOG(LogTemp, Warning, TEXT("=============== WebSocket is Ready! Starting conversation..."));
            PokeServer();    		
            return;
    	}

        // Type: Response
        if (Type == EnumPtr->GetNameStringByIndex(EServerEventType::Response))
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Type: %s"), *Type);
            Manager->bInputEnabled = true;
            ParseResponse(j);        	
            return;
        }

        // Type: SessionStarted
        if (Type == EnumPtr->GetNameStringByIndex(EServerEventType::SessionStarted))
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Type: %s"), *Type);
            return;
        }

        // Type: SessionEnded
        if (Type == EnumPtr->GetNameStringByIndex(EServerEventType::SessionEnded))
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Type: %s"), *Type);
            bSessionEnded = true;
            return;
        }

        // Type: Input Stream Audio Open
        if (Type == EnumPtr->GetNameStringByIndex(EServerEventType::InputAudioStreamOpen))
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Type: %s"), *Type);
            Manager->EnableVoiceCapture();
            bAudioEnabled = true;
            return;
        }

        // Type: Audio Input Recognized
        if (Type == EnumPtr->GetNameStringByIndex(EServerEventType::Recognized))
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Type: %s"), *Type);
            DisableAudioInput();
            Manager->StopRecordingAudio();
            bAudioEnabled = false;
            return;
        }

    }
	
}

void UPromethistWS::OnRawMessage(const TArray<uint8>& Data, int32 BytesRemaining)
{
	
}

void UPromethistWS::OnMessageSent(const FString& Message)
{
    // We just sent a message
    UE_LOG(LogTemp, Warning, TEXT("=============== We just sent %s to the server."), *Message);
}

void UPromethistWS::SendMessage(FString Message)
{
    if (WebSocket)
    {
        WebSocket->SendMessage(Message);
    }
}

void UPromethistWS::SendMessageBinary(TArray<uint8> BinaryMessage, bool Silence)
{
    if (WebSocket && BinaryMessage.Num() >= 0 && bAudioEnabled)
    {
        if (Silence)
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Sending SILENCE audio data!"));
        } else
        {
            UE_LOG(LogTemp, Warning, TEXT("=============== Sending MICROPHONE audio data!"));
        }
        WebSocket->SendRawMessage(BinaryMessage, true);
    }
}

void UPromethistWS::InitConversation(FString AppKey, int SilenceTimeout)
{
    json j;

    j["type"] = "Init";
    j["key"] = TCHAR_TO_UTF8(*AppKey);
    j["appKey"] = TCHAR_TO_UTF8(*AppKey);
    j["deviceId"] = "sender";
    j["config"] = {
        {"locale", "en"},
        {"zoneId", "Europe/Prague"},
        {"sttMode", "SingleUtterance"},
        {"sttSampleRate", 16000},
        {"tts", "RequiredLinks"},
        {"returnSsml", false },
        {"silenceTimeout", SilenceTimeout },
    	{"test", false}   	
    };

    FString Message = FString(j.dump().c_str());
    UE_LOG(LogTemp, Warning, TEXT("=============== Created JSON Message: %s"), *Message);
    WebSocket->SendMessage(Message);

    AppKeyWS = AppKey;
}

void UPromethistWS::PokeServer()
{
    json j;
    UUID = uuid::generate_uuid_v4();

    j["type"] = "Request";
    j["request"] = {
        {"input", {
            {"transcript", {
                {"text", "#Intro"},
                {"confidence", 1}
            }}
        }},
        {"deviceId", "sender"},
        {"appKey", TCHAR_TO_UTF8(*AppKeyWS)},
        {"sessionId", UUID},
        {"attributes", {
            {"clientType", "UE4-PBRDigital"},
            {"clientScreen", true}
        }}
    };

    FString Message = FString(j.dump().c_str());
    UE_LOG(LogTemp, Warning, TEXT("=============== Created JSON Message: %s"), *Message);
    WebSocket->SendMessage(Message);
}

void UPromethistWS::SendEventToServer(FString Event)
{
    json j;
    FString EventName = "#" + Event;

    j["type"] = "Request";
    j["request"] = {
        {"input", {
            {"transcript", {
                {"text", TCHAR_TO_UTF8(*EventName)},
                {"confidence", 1}
            }}
        }},
        {"deviceId", "sender"},
        {"appKey", TCHAR_TO_UTF8(*AppKeyWS)},
        {"sessionId", UUID},
        {"attributes", {
            {"clientType", "UE4-PBRDigital"},
            {"clientScreen", true}
        }}
    };

    FString Message = FString(j.dump().c_str());
    UE_LOG(LogTemp, Warning, TEXT("=============== Created JSON Message: %s"), *Message);
    WebSocket->SendMessage(Message);
}

void UPromethistWS::EnableAudioInput()
{
    if (!bSessionEnded)
    {
        json j;

        j["type"] = "InputAudioStreamOpen";
        j["appKey"] = TCHAR_TO_UTF8(*AppKeyWS);

        FString Message = FString(j.dump().c_str());
        UE_LOG(LogTemp, Warning, TEXT("=============== Created JSON Message: %s"), *Message);
        WebSocket->SendMessage(Message);
    } else
    {
	    // Session ended (end of dialogue?)
    }
}

void UPromethistWS::DisableAudioInput()
{	
    json j;

    j["type"] = "InputAudioStreamClose";
    j["appKey"] = TCHAR_TO_UTF8(*AppKeyWS);

    FString Message = FString(j.dump().c_str());
    UE_LOG(LogTemp, Warning, TEXT("=============== Created JSON Message: %s"), *Message);
    WebSocket->SendMessage(Message);
}

UBlueprintWebSocket* UPromethistWS::GetWebSocket()
{
    return WebSocket;
}

void UPromethistWS::ParseResponse(json j)
{
    if (j["response"].contains("items"))
    {
        j = j["response"]["items"];

        uint32 Count = 0;
    	
        for (json::iterator it = j.begin(); it != j.end(); ++it) {
            if (!it.value()["audio"].is_null())
            {
                Count++;
            }
        }

        Manager->TotalAudioCount = Count;

    	// Run through all "audio" and "command" JSON objects, collect the audio URLs
        for (json::iterator it = j.begin(); it != j.end(); ++it) {
        	if (!it.value()["audio"].is_null())
        	{
                FString AudioUrl = it.value()["audio"].get<std::string>().c_str();
                Manager->FilesToDownload.Enqueue(AudioUrl);
                UE_LOG(LogTemp, Warning, TEXT("=============== Item audio file url: %s"), *AudioUrl);

        		// Lep�� by byl n�jak� Pair, ale whatever
                if (!it.value()["voice"].is_null())
                {
                    FString Voice = it.value()["voice"].get<std::string>().c_str();
                    Manager->Voices.Enqueue(Voice);
                }
        	} else
        	{
                if (!it.value()["text"].is_null())
                {
                    FString TextCommand = it.value()["text"].get<std::string>().c_str();
                	if (TextCommand.Left(1) == "#")
                	{
                        if (TextCommand == "#noinput")
                        {
                            Manager->bInputEnabled = false; // no-input command
                            UE_LOG(LogTemp, Warning, TEXT("=============== Input disabled for this speech"));
                        } else
                        { 
                            Manager->Command = TextCommand; // classic command
                            Manager->OnCommandReceivedEvent.Broadcast(TextCommand);
                        }
                        UE_LOG(LogTemp, Warning, TEXT("=============== New Command form server: %s"), *TextCommand);
                	}
                    
                }
        	}
        }

        Manager->DownloadFile();
    }
}