// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class AudioImporterEditor : ModuleRules
{

	private string ModulePath
	{
		get { return ModuleDirectory; }
	}

	private string ThirdPartyPath
	{
		get { return Path.GetFullPath(Path.Combine(ModulePath, "../ThirdParty/")); }
	}


	public AudioImporterEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		OptimizeCode = CodeOptimization.Never;
		bEnableExceptions = true;

		DynamicallyLoadedModuleNames.AddRange(
			new string[] {
				"Media",
			});
        
		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
				"Engine",
				"MediaUtils",
				"RenderCore",
				"Projects",
				"AudioImporter",
				"UnrealEd",
                "AudioEditor",
                "Settings"
            });


		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"Media",
                "AudioImporter"
            });

		//PrivateIncludePaths.AddRange(
		//	new string[] {
		//		"AudioImporterEditor/Private",
		//	});


  //      PublicIncludePaths.AddRange(
  //          new string[] {
  //              "AudioImporterEditor/Public",
  //          });

		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "Public"));
		PrivateIncludePaths.Add(Path.Combine(ModuleDirectory, "Private"));
	}
}
