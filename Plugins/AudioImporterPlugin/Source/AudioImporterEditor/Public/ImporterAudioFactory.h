// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Factories/SoundFactory.h"
#include <EditorReimportHandler.h>
#include <Factories/Factory.h>
#include <Factories.h>
#include <HAL/FileManager.h>
#include "AudioImporterSettings.h"

#include <stdio.h>

#include "DecoderGeneric.h"

#include "AssetRegistryModule.h"
#include "PackageTools.h"
#include "ImporterAudioFactory.generated.h"

/*
const FString SurroundSpeakerLocations[SPEAKER_Count] =
{
	TEXT("_fl"),		// SPEAKER_FrontLeft
	TEXT("_fr"),		// SPEAKER_FrontRight
	TEXT("_fc"),		// SPEAKER_FrontCenter
	TEXT("_lf"),		// SPEAKER_LowFrequency
	TEXT("_sl"),		// SPEAKER_SideLeft
	TEXT("_sr"),		// SPEAKER_SideRight
	TEXT("_bl"),		// SPEAKER_BackLeft
	TEXT("_br")			// SPEAKER_BackRight
};*/

/**
 * Contains methods to handle file importation into Unreal Engine
 */
UCLASS(MinimalAPI, hidecategories = Object)
class UImporterAudioFactory : public UFactory, public FReimportHandler
{
	GENERATED_BODY()

public:


	/**
	 * Init an audio importer factory
	 */
	UImporterAudioFactory(const FObjectInitializer& ObjectInitializer);

	/**
	 * Init audio factory parameters
	 */
	void PostInitProperties() override;

	/**
	* To know if a class is supported by the audio factory
	* @return true if the class is supported, otherwise false
	*/
	virtual bool DoesSupportClass(UClass * Class) override;

	/**
	* @return the supported class by the audio importer factory (here USoundWave class)
	*/
	virtual UClass* ResolveSupportedClass() override;

	/**
	* Convert an input audio file not supported by engine to a WAVE audio file supported by engine
	* @param InName name of the input audio file
	* @param Type extension of the file
	* @return the USoundWave containing converted audio
	*/
	virtual UObject* FactoryCreateBinary(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, const TCHAR* Type, const uint8*& Buffer, const uint8* BufferEnd, FFeedbackContext* Warn) override;

	/**
	* Return true if this factory can import the file
	* @param filename the file to be considered
	*/
	virtual bool FactoryCanImport(const FString & Filename);
	
	virtual bool CanReimport(UObject* Obj, TArray<FString>& OutFilenames) override;

	virtual void SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths) override;

	virtual EReimportResult::Type Reimport(UObject* Obj) override;

	/**
	* Get priority of this factory on engine
	*/
	virtual int32 GetPriority() const override;

	/**
	* Blueprint node to convert an audio file to an Unreal Engine audio format sound
	* Warning : redondant code with FactoryCreateBinary method when an input file is converted to wave file
	* @param path Absolute or relative path to the sound file to be converted
	* @return pointer to USoundWave created
	*/
	UFUNCTION(BlueprintCallable, Category = "Audio|AudioImporter", meta = (WorldContext = "WorldContextObject"))
	static USoundWave* ConvertAudioFile(UObject* WorldContextObject, FString path);

private:
	/**
	* The last USoundWave created during runtime is saved here
	* Fear that Unreal Engine garbage collector will destroy the created USoundWave during runtime 
	* Probably work without it, but not tested yet
	*/
	static USoundWave* saved;
	USoundWave* sw;



	/*
	UObject* CreateObject
	(
		UClass*				Class,
		UObject*			InParent,
		FName				Name,
		EObjectFlags		Flags,
		UObject*			Context,
		const TCHAR*		FileType,
		const uint8*&		Buffer,
		const uint8*		BufferEnd,
		FFeedbackContext*	Warn
	);

	void UpdateTemplate();
	TWeakObjectPtr<USoundWave> TemplateSoundWave;
	uint8 SuppressImportDialogOptions;*/
};
