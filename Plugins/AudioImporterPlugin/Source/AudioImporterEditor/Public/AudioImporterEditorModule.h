// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

//#include "AudioImporterPrivate.h"

#include "IMediaCaptureSupport.h"
#include "Modules/ModuleManager.h"

#include "Core.h"
#include "Interfaces/IPluginManager.h"

#include "AudioImporterSettings.h"
#include "ISettingsModule.h"
#include "ISettingsSection.h"
#include "ISettingsContainer.h"

#include "IMediaModule.h"
#include "Modules/ModuleInterface.h"
#include "Templates/SharedPointer.h"

#define LOCTEXT_NAMESPACE "AudioImporterSettings"

DECLARE_LOG_CATEGORY_EXTERN(LogAudioImporter, Log, All);


/**
* 
*/
class AudioImporterEditorModule : public IModuleInterface
{
public:
	/**
	* First method called to initialize plugin
	*/
	virtual void StartupModule() override;
	/**
	* Called to clean up plugin
	*/
	virtual void ShutdownModule() override;
	
	virtual bool SupportsDynamicReloading() override
	{
		return true;
	}

	/**
	* Register custom settings in editor settings windows
	*/
	void RegisterSettings();
	/**
	* Unregister custom settings in editor settings windows
	*/
	void UnregisterSettings();

	bool HandleSettingsSaved()
	{

		UAudioImporterSettings* Settings = GetMutableDefault<UAudioImporterSettings>();
		bool ResaveSettings = false;

		// You can put any validation code in here and resave the settings in case an invalid
		// value has been entered

		if (ResaveSettings)
		{
			Settings->SaveConfig();
		}

		return true;
	}
};