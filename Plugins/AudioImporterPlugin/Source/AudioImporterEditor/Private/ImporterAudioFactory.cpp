// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#include "ImporterAudioFactory.h"

#include "AudioImporterEditorModule.h"

#include <Editor.h>
#include <EditorFramework/AssetImportData.h>

#include "WaveEncoder.h"
#include "RawMetadata.h"
#include "AudioImporterUtils.h"

#include "Editor/AudioEditor/Classes/Factories/SoundFactory.h"

#include "Sound/SoundWave.h" 
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>




#include "AssetRegistryModule.h"
#include "Audio.h"
#include "Components/AudioComponent.h"
#include "ContentBrowserModule.h"
#include "IContentBrowserSingleton.h"
#include "Modules/ModuleManager.h"
#include "Sound/SoundCue.h"
#include "Sound/SoundNode.h"
#include "Sound/SoundNodeWavePlayer.h"
#include "Sound/SoundNodeModulator.h"
#include "Sound/SoundNodeAttenuation.h"
#include "Sound/SoundWave.h"
#include "AudioDeviceManager.h"
#include "AudioDevice.h"
#include "AudioEditorModule.h"
#include "Editor.h"
#include "Misc/MessageDialog.h"
#include "Misc/FeedbackContext.h"
#include "EditorFramework/AssetImportData.h"
#include "AudioCompressionSettingsUtils.h"
#include "SoundFileIO/SoundFileIO.h"
#include "Factories/SoundFactory.h"

enum ESuppressImportDialog
{
	None = 0,
	Overwrite = 1 << 0,
	UseTemplate = 1 << 1
};



DEFINE_LOG_CATEGORY(LogAudioImporter);
//#define LOCTEXT_NAMESPACE "AudioImporterEditor"

USoundWave* UImporterAudioFactory::saved = NewObject<USoundWave>();





/*


UObject* UImporterAudioFactory::CreateObject
(
	UClass*				Class,
	UObject*			InParent,
	FName				Name,
	EObjectFlags		Flags,
	UObject*			Context,
	const TCHAR*		FileType,
	const uint8*&		Buffer,
	const uint8*		BufferEnd,
	FFeedbackContext*	Warn
)
{
	FString CuePackageSuffix = TEXT("_Cue");
	bool bAutoCreateCue = false;

	// create the group name for the cue
	const FString GroupName = InParent->GetFullGroupName(false);
	FString CuePackageName = InParent->GetOutermost()->GetName();
	CuePackageName += CuePackageSuffix;
	if (GroupName.Len() > 0 && GroupName != TEXT("None"))
	{
		CuePackageName += TEXT(".");
		CuePackageName += GroupName;
	}

	// validate the cue's group
	FText Reason;
	const bool bCuePathIsValid = FName(*CuePackageSuffix).IsValidGroupName(Reason);
	const bool bMoveCue = CuePackageSuffix.Len() > 0 && bCuePathIsValid && bAutoCreateCue;
	if (bAutoCreateCue)
	{
		if (!bCuePathIsValid)
		{
			FMessageDialog::Open(EAppMsgType::Ok, FText::Format(NSLOCTEXT("SoundFactory", "Import Failed", "Import failed for {0}: {1}"), FText::FromString(CuePackageName), Reason));
			GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
			return nullptr;
		}
	}

	// if we are creating the cue move it when necessary
	UPackage* CuePackage = bMoveCue ? CreatePackage(nullptr, *CuePackageName) : nullptr;

	// if the sound already exists, remember the user settings
	USoundWave* ExistingSound = FindObject<USoundWave>(InParent, *Name.ToString());

	TArray<UAudioComponent*> ComponentsToRestart;
	FAudioDeviceManager* AudioDeviceManager = GEngine->GetAudioDeviceManager();
	if (AudioDeviceManager && ExistingSound)
	{
		// Will block internally on audio thread completing outstanding commands
		AudioDeviceManager->StopSoundsUsingResource(ExistingSound, &ComponentsToRestart);

		// We need to clear out any stale multichannel data on the sound wave in the case this is a reimport from multichannel to mono/stereo
		ExistingSound->ChannelOffsets.Reset();
		ExistingSound->ChannelSizes.Reset();
		ExistingSound->bIsAmbisonics = false;

		// Resource data is required to exist, if it hasn't been loaded yet,
		// to properly flush compressed data.  This allows the new version
		// to be auditioned in the editor properly.
		if (!ExistingSound->ResourceData)
		{
			if (FAudioDevice* AudioDevice = GEngine->GetMainAudioDevice())
			{
				FName RuntimeFormat = AudioDevice->GetRuntimeFormat(ExistingSound);
				ExistingSound->InitAudioResource(RuntimeFormat);
			}
		}

		UE_LOG(LogAudioEditor, Log, TEXT("Stopping Sound Resources of Existing Sound"));
		if (ComponentsToRestart.Num() > 0)
		{
			for (UAudioComponent* AudioComponent : ComponentsToRestart)
			{
				UE_LOG(LogAudioEditor, Log, TEXT("Component '%s' Stopped"), *AudioComponent->GetName());
				AudioComponent->Stop();
			}
		}
	}

	UpdateTemplate();

	bool bUseExistingSettings = SuppressImportDialogOptions & ESuppressImportDialog::Overwrite;
	if (ExistingSound && !bUseExistingSettings && !GIsAutomationTesting)
	{
		SuppressImportDialogOptions |= ESuppressImportDialog::Overwrite;
		DisplayOverwriteOptionsDialog(FText::Format(
			NSLOCTEXT("SoundFactory", "ImportOverwriteWarning", "You are about to import '{0}' over an existing sound."),
			FText::FromName(Name)));

		switch (OverwriteYesOrNoToAllState)
		{

		case EAppReturnType::Yes:
		case EAppReturnType::YesAll:
		{
			// Overwrite existing settings
			bUseExistingSettings = false;
			break;
		}
		case EAppReturnType::No:
		case EAppReturnType::NoAll:
		{
			// Preserve existing settings
			bUseExistingSettings = true;
			break;
		}
		default:
		{
			GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
			return nullptr;
		}
		}
	}

	// See if this may be an ambisonics import by checking ambisonics naming convention (ambix)
	FString RootName = Name.GetPlainNameString();
	FString AmbiXTag = RootName.Right(6).ToLower();
	FString FuMaTag = RootName.Right(5).ToLower();

	// check for AmbiX or FuMa tag for the file
	bool bIsAmbiX = (AmbiXTag == TEXT("_ambix"));
	bool bIsFuMa = (FuMaTag == TEXT("_fuma"));

	TArray<uint8> RawWaveData;
	RawWaveData.Empty(BufferEnd - Buffer);
	RawWaveData.AddUninitialized(BufferEnd - Buffer);
	FMemory::Memcpy(RawWaveData.GetData(), Buffer, RawWaveData.Num());

	// Read the wave info and make sure we have valid wave data
	FWaveModInfo WaveInfo;
	FString ErrorMessage;
	if (WaveInfo.ReadWaveInfo(RawWaveData.GetData(), RawWaveData.Num(), &ErrorMessage))
	{
		// Validate if somebody has used the ambiX or FuMa tag that the ChannelCount is 4 channels
		if ((bIsAmbiX || bIsFuMa) && (int32)*WaveInfo.pChannels != 4)
		{
			Warn->Logf(ELogVerbosity::Error, TEXT("Tried to import ambisonics format file but requires exactly 4 channels: '%s'"), *Name.ToString());
			GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
			return nullptr;
		}

		// If we are not using libSoundFile, we cannot support non-16 bit WAV files.
		if (*WaveInfo.pBitsPerSample != 16)
		{
#if !WITH_SNDFILE_IO
			WaveInfo.ReportImportFailure();
			Warn->Logf(ELogVerbosity::Error, TEXT("Only 16 bit WAV source files are supported (%s) on this editor platform."), *Name.ToString());
			GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
#endif // WITH_SNDFILE_IO

			return nullptr;
		}
	}
	else
	{
		Warn->Logf(ELogVerbosity::Error, TEXT("Unable to read wave file '%s' - \"%s\""), *Name.ToString(), *ErrorMessage);
		GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
		return nullptr;
	}


	// Use pre-existing sound if it exists and we want to keep settings,
	// otherwise create new sound and import raw data.
	USoundWave* Sound = (bUseExistingSettings && ExistingSound) ? ExistingSound : NewObject<USoundWave>(InParent, Name, Flags, TemplateSoundWave.Get());

	// These get wiped in PostInitProperties by defaults set from Audio Settings,
	// so set back to template in this specialized case
	if (TemplateSoundWave.IsValid())
	{
		Sound->SoundClassObject = TemplateSoundWave->SoundClassObject;
		Sound->ConcurrencySet = TemplateSoundWave->ConcurrencySet;
	}

	// If we're a multi-channel file, we're going to spoof the behavior of the SoundSurroundFactory
	int32 ChannelCount = (int32)*WaveInfo.pChannels;
	check(ChannelCount > 0);

	int32 SizeOfSample = (*WaveInfo.pBitsPerSample) / 8;

	int32 NumSamples = WaveInfo.SampleDataSize / SizeOfSample;
	int32 NumFrames = NumSamples / ChannelCount;

	if (ChannelCount > 2)
	{
		// We need to deinterleave the raw PCM data in the multi-channel file reuse a scratch buffer
		TArray<int16> DeinterleavedAudioScratchBuffer;

		// Store the array of raw .wav files we're going to create from the deinterleaved int16 data
		TArray<uint8> RawChannelWaveData[SPEAKER_Count];

		// Ptr to the pcm data of the imported sound wave
		int16* SampleDataBuffer = (int16*)WaveInfo.SampleDataStart;

		int32 TotalSize = 0;

		Sound->ChannelOffsets.Empty(SPEAKER_Count);
		Sound->ChannelOffsets.AddZeroed(SPEAKER_Count);

		Sound->ChannelSizes.Empty(SPEAKER_Count);
		Sound->ChannelSizes.AddZeroed(SPEAKER_Count);

		TArray<int32> ChannelIndices;
		if (ChannelCount == 4)
		{
			ChannelIndices = {
				SPEAKER_FrontLeft,
				SPEAKER_FrontRight,
				SPEAKER_LeftSurround,
				SPEAKER_RightSurround
			};
		}
		else if (ChannelCount == 6)
		{
			ChannelIndices = {
				SPEAKER_FrontLeft,
				SPEAKER_FrontRight,
				SPEAKER_FrontCenter,
				SPEAKER_LowFrequency,
				SPEAKER_LeftSurround,
				SPEAKER_RightSurround
			};
		}
		else if (ChannelCount == 8)
		{
			ChannelIndices = {
				SPEAKER_FrontLeft,
				SPEAKER_FrontRight,
				SPEAKER_FrontCenter,
				SPEAKER_LowFrequency,
				SPEAKER_LeftSurround,
				SPEAKER_RightSurround,
				SPEAKER_LeftBack,
				SPEAKER_RightBack
			};
		}
		else
		{
			Warn->Logf(ELogVerbosity::Error, TEXT("Wave file '%s' has unsupported number of channels %d"), *Name.ToString(), ChannelCount);
			GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
			return nullptr;
		}

		// Make some new sound waves
		check(ChannelCount == ChannelIndices.Num());
		for (int32 Chan = 0; Chan < ChannelCount; ++Chan)
		{
			// Build the deinterleaved buffer for the channel
			DeinterleavedAudioScratchBuffer.Empty(NumFrames);
			for (int32 Frame = 0; Frame < NumFrames; ++Frame)
			{
				const int32 SampleIndex = Frame * ChannelCount + Chan;
				DeinterleavedAudioScratchBuffer.Add(SampleDataBuffer[SampleIndex]);
			}

			// Now create a sound wave asset
			SerializeWaveFile(RawChannelWaveData[Chan], (uint8*)DeinterleavedAudioScratchBuffer.GetData(), NumFrames * sizeof(int16), 1, *WaveInfo.pSamplesPerSec);

			// The current TotalSize is the "offset" into the bulk data for this sound wave
			Sound->ChannelOffsets[ChannelIndices[Chan]] = TotalSize;

			// "ChannelSize" is the size of the .wav file representing this channel of data
			const int32 ChannelSize = RawChannelWaveData[Chan].Num();

			// Store it in the sound wave
			Sound->ChannelSizes[ChannelIndices[Chan]] = ChannelSize;

			// TotalSize is the sum of all ChannelSizes
			TotalSize += ChannelSize;
		}

		// Now we have an array of mono .wav files in the format that the SoundSurroundFactory expects
		// copy the data into the bulk byte data

		// Get the raw data bulk byte pointer and copy over the .wav files we generated
		Sound->RawData.Lock(LOCK_READ_WRITE);

		uint8* LockedData = (uint8*)Sound->RawData.Realloc(TotalSize);
		int32 RawDataOffset = 0;


		if (bIsAmbiX || bIsFuMa)
		{
			check(ChannelCount == 4);

			// Flag that this is an ambisonics file
			Sound->bIsAmbisonics = true;
		}
		for (int32 Chan = 0; Chan < ChannelCount; ++Chan)
		{
			const int32 ChannelSize = RawChannelWaveData[Chan].Num();
			FMemory::Memcpy(LockedData + RawDataOffset, RawChannelWaveData[Chan].GetData(), ChannelSize);
			RawDataOffset += ChannelSize;
		}

		Sound->RawData.Unlock();
	}
	else
	{
		// For mono and stereo assets, just copy the data into the buffer
		Sound->RawData.Lock(LOCK_READ_WRITE);
		void* LockedData = Sound->RawData.Realloc(BufferEnd - Buffer);
		FMemory::Memcpy(LockedData, Buffer, BufferEnd - Buffer);
		Sound->RawData.Unlock();
	}

	Sound->Duration = (float)NumFrames / *WaveInfo.pSamplesPerSec;
	Sound->SetSampleRate(*WaveInfo.pSamplesPerSec);
	Sound->NumChannels = ChannelCount;
	Sound->TotalSamples = *WaveInfo.pSamplesPerSec * Sound->Duration;

	// Store the current file path and timestamp for re-import purposes
	Sound->AssetImportData->Update(CurrentFilename);

	// Compressed data is now out of date.
	Sound->InvalidateCompressedData(true); // bFreeResources

	// If stream caching is enabled, we need to make sure this asset is ready for playback.
	if (FPlatformCompressionUtilities::IsCurrentPlatformUsingStreamCaching() && Sound->IsStreaming())
	{
		Sound->EnsureZerothChunkIsLoaded();
	}

	GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, Sound);

	if (ExistingSound && bUseExistingSettings)
	{
		// Call PostEditChange() to update text to speech
		Sound->PostEditChange();
	}

	// if we're auto creating a default cue
	//if (bAutoCreateCue)
	//{
	//	CreateSoundCue(Sound, bMoveCue ? CuePackage : InParent, Flags, bIncludeAttenuationNode, bIncludeModulatorNode, bIncludeLoopingNode, CueVolume);
	//}

	for (UAudioComponent* AudioComponent : ComponentsToRestart)
	{
		AudioComponent->Play();
	}

	Sound->bNeedsThumbnailGeneration = true;

	return Sound;
}


void UImporterAudioFactory::UpdateTemplate()
{
	if (!IsAutomatedImport() && !TemplateSoundWave.IsValid() && !(SuppressImportDialogOptions & ESuppressImportDialog::UseTemplate))
	{
		SuppressImportDialogOptions |= ESuppressImportDialog::UseTemplate;

		FContentBrowserModule& ContentBrowserModule = FModuleManager::LoadModuleChecked<FContentBrowserModule>("ContentBrowser");
		TArray<FAssetData> SelectedAssets;
		ContentBrowserModule.Get().GetSelectedAssets(SelectedAssets);

		if (SelectedAssets.Num() == 1)
		{
			if (USoundWave* SoundWave = Cast<USoundWave>(SelectedAssets[0].GetAsset()))
			{
				const bool bUseTemplateSoundWave = FMessageDialog::Open(EAppMsgType::YesNo, FText::Format(
					NSLOCTEXT("SoundFactory", "UseSoundWaveTemplate", "Use the selected Sound Wave '{0}' in the Content Browser as a template for sound(s) being imported?"),
					FText::FromString(SoundWave->GetName()))) == EAppReturnType::Yes;

				if (bUseTemplateSoundWave)
				{
					TemplateSoundWave = SoundWave;
				}
			}
		}
	}
}

*/




UImporterAudioFactory::UImporterAudioFactory(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

	SupportedClass = USoundWave::StaticClass();
	//Add file extensions to be supported by the plugin
	Formats.Add(TEXT("mp3;MPEG-2 Audio"));
	Formats.Add(TEXT("m4a;AAC Audio with m4a extension"));
	Formats.Add(TEXT("aac;AAC Audio"));
	Formats.Add(TEXT("wma;Windows Media File"));
	
	ImportPriority++;
	
	bCreateNew = false;
	bEditorImport = true;
	
}

void UImporterAudioFactory::PostInitProperties()
{
	Super::PostInitProperties();
	//ImportOption = NewObject<UVoxImportOption>(this, NAME_None, RF_NoFlags);
}

bool UImporterAudioFactory::FactoryCanImport(const FString & Filename)
{
	return true;
}


bool UImporterAudioFactory::DoesSupportClass(UClass * Class)
{
	return true;
}

UClass* UImporterAudioFactory::ResolveSupportedClass()
{
	return USoundWave::StaticClass();
}


UObject* UImporterAudioFactory::FactoryCreateBinary(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, const TCHAR* Type, const uint8*& Buffer, const uint8* BufferEnd, FFeedbackContext* Warn)
{


	FString nameFString = InName.ToString();
	FString* indicator = new FString();
	nameFString.Split("_", nullptr, indicator);

	FString inputPath = GetCurrentFilename();
	FString outfile = FString(InName.ToString() + ".wav");
	FString rawFile = FString(AudioImporterUtils::GetSystemTempDirectory() + InName.ToString() + ".raw");

	FBufferReader reader = FBufferReader((void*)Buffer, BufferEnd - Buffer, false);

	//FEditorDelegates::OnAssetPreImport.Broadcast(this, InClass, InParent, InName, Type);
	UImportSubsystem* importSystem = GEditor->GetEditorSubsystem<UImportSubsystem>();
	importSystem->BroadcastAssetPreImport(this, InClass, InParent, InName, Type);
	//Conversion procedure

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	TArray<FString> AssetData;


	FString extension = FString(Type);

	TSharedPtr<Decoder, ESPMode::ThreadSafe> decoder;
	//if(extension.Equals("mp3"))
	//	decoder = TSharedPtr<Decoder, ESPMode::ThreadSafe>(new MP3Decoder(inputPath));
		//else if(extension.Equals("m4a") || extension.Equals("aac"))
	decoder = TSharedPtr<Decoder, ESPMode::ThreadSafe>(new DecoderGeneric(inputPath, rawFile)); //Universal decoder

	RawMetadata metadata;
	try {
		metadata = decoder->Decode();
	}
	catch (const char* ex)
	{
		FString exStr = FString(ex);
		UE_LOG(LogAudioImporter, Error, TEXT("Error while decoding input file : %s"), *exStr);
		return nullptr;
	}

	FString fmtStr = FString(metadata.format);
	FString outfileNameStr = FString(metadata.outfileName);
	int nchannels = metadata.nChannels;
	int sampleRate = metadata.sampleRate;

	TUniquePtr<WaveEncoder> encoder(new WaveEncoder(outfile, rawFile, metadata));

	try
	{
		encoder->Encode();
	}
	catch (const char* ex)
	{
		FString exStr = FString(ex);
		UE_LOG(LogAudioImporter, Error, TEXT("Error while encoding WAVE : %s"), *exStr);
		return nullptr;
	}

	//End conversion

	std::string inputfilename = TCHAR_TO_UTF8(*FPaths::ProjectContentDir());
	inputfilename.append(TCHAR_TO_UTF8(*outfile));


	//Load wave data
	std::ifstream input(inputfilename.c_str(), std::ios::binary);
	std::vector<unsigned char> buffer = std::vector<unsigned char>(std::istreambuf_iterator<char>(input), {});
	TArray<uint8> bufferarray = TArray<uint8>();
	for (int i = 0; i < buffer.size(); i++)
		bufferarray.Add(buffer[i]);
	
	//Construct USoundWave object

	//If the input file is a multi-canal file we have a particular treatment to construct USoundWave object
	
	//if (indicator->Compare("lf") == 0 || indicator->Compare("bl") == 0 || indicator->Compare("br") == 0 || indicator->Compare("fc") == 0 || indicator->Compare("fl") == 0 || indicator->Compare("fr") == 0 || indicator->Compare("sl") == 0 || indicator->Compare("sr") == 0)
	//{

	//	//Sound surround management
	//	int32 SpeakerIndex;
	//	FString RootName = InName.GetPlainNameString();
	//	FString SpeakerLocation = RootName.Right(3).ToLower();
	//	FName BaseName = FName(*RootName.LeftChop(3));

	//	// NOT WORKING IN 4.26
	//	/*
	//	for (SpeakerIndex = 0; SpeakerIndex < SPEAKER_Count; SpeakerIndex++)
	//	{
	//		if (SpeakerLocation == SurroundSpeakerLocations[SpeakerIndex]) break;
	//	}
	//	*/

	//	if (SpeakerIndex == SPEAKER_Count)
	//	{
	//		Warn->Logf(ELogVerbosity::Error, TEXT("Failed to find speaker location. No valid extension"));
	//		//FEditorDelegates::OnAssetPostImport is deprecated
	//		//UImportSubsystem* importSubsystem = GEditor->GetEditorSubsystem<UImportSubsystem>();
	//		//importSubsystem->BroadcastAssetPostImport(this, nullptr);
	//		GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
	//		
	//		return(nullptr);
	//	}

	//	//Find existing soundwave
	//	USoundWave* Sound = FindObject<USoundWave>(InParent, *BaseName.ToString());

	//	//Create new sound if necessary
	//	if (Sound == nullptr)
	//	{
	//		//If this is a single asset package, then create package so that its name will be identical to the asset.
	//		if (PackageTools::IsSingleAssetPackage(InParent->GetName()))
	//		{
	//			InParent = CreatePackage(*InParent->GetName().LeftChop(3));

	//			//Make sure the destination package is loaded
	//			CastChecked<UPackage>(InParent)->FullyLoad();
	//			Sound = FindObject<USoundWave>(InParent, *BaseName.ToString());
	//		}

	//		if (Sound == nullptr)
	//			Sound = NewObject<USoundWave>(InParent, BaseName, Flags);
	//	}

	//	

	//	//Clear resources so that if it's already been played, it will reload the wave data
	//	Sound->FreeResources();

	//	//Presize the offsets array, in case the sound was new or the original sound data was stripped by cooking
	//	if (Sound->ChannelOffsets.Num() != SPEAKER_Count)
	//	{
	//		Sound->ChannelOffsets.Empty(SPEAKER_Count);
	//		Sound->ChannelOffsets.AddZeroed(SPEAKER_Count);
	//	}

	//	//Presize the sizes array, in case the sound was new or the original sound data was stripped by cook
	//	if (Sound->ChannelSizes.Num() != SPEAKER_Count)
	//	{
	//		Sound->ChannelSizes.Empty(SPEAKER_Count);
	//		Sound->ChannelSizes.AddZeroed(SPEAKER_Count);
	//	}

	//	//Store the current file path and timestamp for re-import purposes
	//	Sound->AssetImportData->Update(CurrentFilename);

	//	//Compressed data is now out of date
	//	Sound->InvalidateCompressedData();

	//	//Delete the old version of the wave from the bulk data
	//	uint8* RawWaveData[SPEAKER_Count] = { nullptr };
	//	uint8* RawData = (uint8*)Sound->RawData.Lock(LOCK_READ_WRITE);
	//	int32 RawDataOffset = 0;
	//	int32 TotalSize = 0;

	//	//Copy off the still used waves
	//	for (int i = 0; i < SPEAKER_Count; i++)
	//	{
	//		if (i != SpeakerIndex && Sound->ChannelSizes[i])
	//		{
	//			RawWaveData[i] = new uint8[Sound->ChannelSizes[i]];
	//			FMemory::Memcpy(RawWaveData[i], RawData + Sound->ChannelOffsets[i], Sound->ChannelSizes[i]);
	//			TotalSize += Sound->ChannelSizes[i];
	//		}
	//	}

	//	//Copy them back without the one that will be updated
	//	RawData = (uint8*)Sound->RawData.Realloc(TotalSize);

	//	for (int i = 0; i < SPEAKER_Count; i++)
	//	{
	//		if (RawWaveData[i])
	//		{
	//			FMemory::Memcpy(RawData + RawDataOffset, RawWaveData[i], Sound->ChannelSizes[i]);
	//			Sound->ChannelOffsets[i] = RawDataOffset;
	//			RawDataOffset += Sound->ChannelSizes[i];

	//			delete[] RawWaveData[i];
	//		}
	//	}

	//	uint32 RawDataSize = bufferarray.Num();
	//	uint8* LockedData = (uint8*)Sound->RawData.Realloc(RawDataOffset + RawDataSize);
	//	LockedData += RawDataOffset;
	//	FMemory::Memcpy(LockedData, bufferarray.GetData(), RawDataSize);

	//	Sound->ChannelOffsets[SpeakerIndex] = RawDataOffset;
	//	Sound->ChannelSizes[SpeakerIndex] = RawDataSize;

	//	Sound->RawData.Unlock();

	//	//Calculate duration
	//	FWaveModInfo WaveInfo;
	//	FString ErrorReason;
	//	if (WaveInfo.ReadWaveInfo(LockedData, RawDataSize, &ErrorReason))
	//	{
	//		//Calculate duration in seconds
	//		int32 DurationDiv = *WaveInfo.pChannels * *WaveInfo.pBitsPerSample * *WaveInfo.pSamplesPerSec;
	//		if (DurationDiv)
	//			Sound->Duration = *WaveInfo.pWaveDataSize * 8.0f / DurationDiv;
	//		else
	//			Sound->Duration = 0.0f;

	//		if (*WaveInfo.pBitsPerSample != 16)
	//		{
	//			Warn->Logf(ELogVerbosity::Error, TEXT("Currently, only 16 bit WAV files are supported (%s)."), *InName.ToString());
	//			Sound->MarkPendingKill();
	//			Sound = nullptr;
	//		}

	//		if (*WaveInfo.pChannels != 1)
	//		{
	//			Warn->Logf(ELogVerbosity::Error, TEXT("Currently, only mono WAV files can be imported as channels of surround audio (%s)."), *InName.ToString());
	//			Sound->MarkPendingKill();
	//			Sound = nullptr;
	//		}
	//	}
	//	else
	//	{
	//		Warn->Logf(ELogVerbosity::Error, TEXT("Unable to read wave file '%s' - \"%s\""), *InName.ToString(), *ErrorReason);
	//		Sound->MarkPendingKill();
	//		Sound = nullptr;
	//	}
	//	if (Sound)
	//	{
	//		Sound->NumChannels = 0;
	//		for (int i = 0; i < SPEAKER_Count; i++)
	//		{
	//			if (Sound->ChannelSizes[i])
	//			{
	//				Sound->NumChannels++;
	//			}
	//		}
	//	}

	//	sw = Sound;
	//	
	//	//UImportSubsystem* importSubsystem = GEditor->GetEditorSubsystem<UImportSubsystem>();
	//	//importSubsystem->BroadcastAssetPostImport(this, sw);
	//	GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, sw);
	//	
	//	//return(Sound);
	//}
	////Else we call the generic wav/USoundWave converter
	//else

	
		
		//Old version
		sw = NewObject<USoundWave>(InParent, InName, Flags);
		AudioImporterUtils utils;
		utils.GetSoundWaveFromRawWav(bufferarray, sw, inputPath);


		/*USoundFactory* soundFactory = NewObject<USoundFactory>();
		const uint8* last = &bufferarray.Last(0);
		
		const uint8* top = bufferarray.GetData();
		UObject* resSw = soundFactory->FactoryCreateBinary(InClass, InParent, InName, Flags, Context, TEXT("WAV"), top, last, Warn);
		*/
		//GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, sw);
		
		//FEditorDelegates::OnAssetPostImport.Broadcast(this,sw);


		/* Alternative method

		const uint8* last = &bufferarray.Last(0);
		const uint8* top = bufferarray.GetData();
		GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPreImport(this, InClass, InParent, InName, Type);

		UObject* SoundObject = nullptr;

		SoundObject = CreateObject(InClass, InParent, InName, Flags, Context, Type, top, last, Warn);

	
		if (!SoundObject)
		{
			// Unrecognized sound format
			Warn->Logf(ELogVerbosity::Error, TEXT("Unrecognized sound format '%s' in %s"), Type, *InName.ToString());
			GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPostImport(this, nullptr);
		}

		return SoundObject;

		*/

	
	

	
	input.close();


	//Deleting intermediate files
	
	//Remove raw file
	std::string rawFilestdStr = TCHAR_TO_UTF8(*rawFile);
	if (remove(rawFilestdStr.c_str()) != 0)
	{
		UE_LOG(LogAudioImporter, Log, TEXT("Error during deleting raw file"));
	}

	std::string waveFilestdStr = TCHAR_TO_UTF8(*IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*FPaths::ProjectContentDir()));
	waveFilestdStr.append(TCHAR_TO_UTF8(*outfile));

	//Remove wave file
	FString waveFile = FPaths::ProjectContentDir().Append(outfile);
	FString waveAbsolutePath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*waveFile);
	if (!FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*waveAbsolutePath))
	{
		UE_LOG(LogAudioImporter, Log, TEXT("Could Not Find File"));
	}

	return sw;
}

bool UImporterAudioFactory::CanReimport(UObject* Obj, TArray<FString>& OutFilenames)
{

	USoundWave* SoundWave = Cast<USoundWave>(Obj);
	if (SoundWave)
	{
		SoundWave->AssetImportData->ExtractFilenames(OutFilenames);
		return true;
	}
	return false;
}

void UImporterAudioFactory::SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths)
{
	USoundWave* SoundWave = Cast<USoundWave>(Obj);
	if (SoundWave)
	{
		SoundWave->AssetImportData->UpdateFilenameOnly(NewReimportPaths[0]);
	}
}

EReimportResult::Type UImporterAudioFactory::Reimport(UObject* Obj)
{
	if (Obj == nullptr || Cast<USoundWave>(Obj) == nullptr)
	{
		return EReimportResult::Failed;
	}

	USoundWave* SoundWave = Cast<USoundWave>(Obj);

	const FString Filename = SoundWave->AssetImportData->GetFirstFilename();
	const FString FileExtension = FPaths::GetExtension(Filename);

	if (FileExtension.Compare("MP3", ESearchCase::IgnoreCase) != 0)
		return EReimportResult::Failed;

	if (UFactory::StaticImportObject(SoundWave->GetClass(), SoundWave->GetOuter(), *SoundWave->GetName(), RF_Public | RF_Standalone, *Filename, nullptr, this))
	{

		SoundWave->AssetImportData->Update(Filename);
		SoundWave->MarkPackageDirty();

		return EReimportResult::Succeeded;
	}
	else
	{
		UE_LOG(LogAudioImporter, Log, TEXT("Reimport failed!"));
		return EReimportResult::Failed;
	}

}

int32 UImporterAudioFactory::GetPriority() const
{
	return ImportPriority;
}


USoundWave* UImporterAudioFactory::ConvertAudioFile(UObject* WorldContextObject, FString path)
{

	FString inputPath = path;

	std::string stdpathString = TCHAR_TO_UTF8(*inputPath);
	std::experimental::filesystem::path pathToTest(stdpathString); // Construct the path from a string.
	if (pathToTest.is_relative()) {
		FString withInputPath = FPaths::ProjectContentDir() + path;
		FString FullPath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*withInputPath);
		inputPath = FullPath;
	}
	
	FString outfile = FString(inputPath + ".wav");
	FString rawFile = FString(AudioImporterUtils::GetSystemTempDirectory() + "temp.raw");


	//Conversion procedure

	TSharedPtr<Decoder, ESPMode::ThreadSafe> decoder;
	//if(extension.Equals("mp3"))
	//	decoder = TSharedPtr<Decoder, ESPMode::ThreadSafe>(new MP3Decoder(inputPath));
		//else if(extension.Equals("m4a") || extension.Equals("aac"))
	decoder = TSharedPtr<Decoder, ESPMode::ThreadSafe>(new DecoderGeneric(inputPath, rawFile));

	RawMetadata metadata;
	try {
		metadata = decoder->Decode();
	}
	catch (const char* ex)
	{
		FString exStr = FString(ex);
		UE_LOG(LogAudioImporter, Log, TEXT("Error while decoding input audio : %s"), *exStr);
		return nullptr;
	}

	FString fmtStr = FString(metadata.format);
	FString outfileNameStr = FString(metadata.outfileName);
	int nchannels = metadata.nChannels;
	int sampleRate = metadata.sampleRate;
	UE_LOG(LogAudioImporter, Log, TEXT("Input audio decoded with :\nformat : %s\nChannels : %i\nSample rate : %i\nOutfile : %s"), *fmtStr, nchannels, sampleRate, *outfileNameStr);

	TUniquePtr<WaveEncoder> encoder(new WaveEncoder(outfile, rawFile, metadata));

	try
	{
		encoder->Encode();
	}
	catch (const char* ex)
	{
		FString exStr = FString(ex);
		UE_LOG(LogAudioImporter, Log, TEXT("Error while encoding WAVE : %s"), *exStr);
	}
	//End conversion

	std::string inputfilename = TCHAR_TO_UTF8(*outfile);
	//std::string inputfilename = TCHAR_TO_UTF8(*FPaths::GameContentDir());
	//inputfilename.append(TCHAR_TO_UTF8(*outfile));

	std::ifstream input(inputfilename.c_str(), std::ios::binary);
	std::vector<unsigned char> buffer = std::vector<unsigned char>(std::istreambuf_iterator<char>(input), {});
	TArray<uint8> bufferarray = TArray<uint8>();
	for (int i = 0; i < buffer.size(); i++)
		bufferarray.Add(buffer[i]);



	// old method
	//sw = NewObject<USoundWave>(USoundWave::StaticClass());
	//static USoundWave* saved;
	saved = NewObject<USoundWave>();
	AudioImporterUtils utils;
	utils.GetSoundWaveFromRawWav(bufferarray, saved,path);
	//USoundWave* result = UImporterAudioFactory::StaticGetSoundWaveFromRawWav(bufferarray);
	

	//New method
	/*const uint8* last = &bufferarray.Last(0);
	const uint8* top = bufferarray.GetData();

	UObject* SoundObject = nullptr;

	SoundObject = CreateObject(InClass, InParent, InName, Flags, Context, Type, top, last, Warn);


	if (!SoundObject)
	{
		// Unrecognized sound format
		Warn->Logf(ELogVerbosity::Error, TEXT("Unrecognized sound format '%s' in %s"), Type, *InName.ToString());
	}

	return SoundObject;*/



	input.close();

	
	//Remove raw file
	std::string rawFilestdStr = TCHAR_TO_UTF8(*rawFile);
	if (remove(rawFilestdStr.c_str()) != 0)
	{
		UE_LOG(LogAudioImporter, Log, TEXT("Error during deleting raw file"));
	}

	std::string waveFilestdStr = TCHAR_TO_UTF8(*IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*FPaths::ProjectContentDir()));
	waveFilestdStr.append(TCHAR_TO_UTF8(*outfile));

	//Remove wave file
	FString waveFile = FPaths::ProjectContentDir().Append(outfile);
	FString waveAbsolutePath = outfile;// IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*waveFile);
	if (!FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*waveAbsolutePath))
	{
		UE_LOG(LogAudioImporter, Log, TEXT("Could Not Find File"));
	}
	
	return saved;
}