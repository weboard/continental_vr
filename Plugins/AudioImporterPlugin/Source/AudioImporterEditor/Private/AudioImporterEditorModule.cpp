// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#include "AudioImporterEditorModule.h"

#include "AssetToolsModule.h"
#include "Factories/SoundFactory.h"
#include "ObjectTools.h"
#include "PackageTools.h"


UObject* AttemptImport(UClass* InFactoryType, UPackage* Package, FName InName, bool& bCancelled, const FString& FullFilename)
{
	UObject* Asset = nullptr;

	if (UFactory* Factory = NewObject<UFactory>(GetTransientPackage(), InFactoryType))
	{
		Factory->AddToRoot();
		if (Factory->ConfigureProperties())
		{
			if (auto* SupportedClass = Factory->ResolveSupportedClass())
			{
				try {
					Asset = Factory->ImportObject(SupportedClass, Package, InName, RF_Public | RF_Standalone, FullFilename, nullptr, bCancelled);
				}
				catch (...)
				{
//					UE_LOG(LogAudioImporter,Log, TEXT("Error during AttemptImport"));
				}
			}
		}
		Factory->RemoveFromRoot();
	}

	return Asset;
}

void AudioImporterEditorModule::StartupModule()
{
	//Register custom settings
	RegisterSettings();

}

void AudioImporterEditorModule::ShutdownModule()
{
	UnregisterSettings();
}

void AudioImporterEditorModule::RegisterSettings()
{
	// Registering some settings is just a matter of exposing the default UObject of
	// your desired class, feel free to add here all those settings you want to expose
	// to your LDs or artists.

	if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		// Create the new category
		ISettingsContainerPtr SettingsContainer = SettingsModule->GetContainer("Project");

		SettingsContainer->DescribeCategory("Plugins",
			LOCTEXT("RuntimeWDCategoryName", "Plugins"),
			LOCTEXT("RuntimeWDCategoryDescription", "Configuration for the Audio Importer plugin"));

		// Register the settings
		ISettingsSectionPtr SettingsSection = SettingsModule->RegisterSettings("Project", "Plugins", "AudioImporterSettings",
			LOCTEXT("RuntimeGeneralSettingsName", "Audio Importer"),
			LOCTEXT("RuntimeGeneralSettingsDescription", "Base configuration for plugin"),
			GetMutableDefault<UAudioImporterSettings>()
		);

		// Register the save handler to your settings, you might want to use it to
		// validate those or just act to settings changes.
		if (SettingsSection.IsValid())
		{
			SettingsSection->OnModified().BindRaw(this, &AudioImporterEditorModule::HandleSettingsSaved);
		}
	}
}

void AudioImporterEditorModule::UnregisterSettings()
{
	// Ensure to unregister all of your registered settings here, hot-reload would
	// otherwise yield unexpected results.

	if (ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
	{
		SettingsModule->UnregisterSettings("Project", "Plugins", "AudioImporterSettings");
	}
}

IMPLEMENT_MODULE(AudioImporterEditorModule, AudioImporterEditor);