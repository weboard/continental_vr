// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "Logging/LogMacros.h"


/** Log category for the WmfMedia module. */
DECLARE_LOG_CATEGORY_EXTERN(LogAudioImporter, Log, All);
