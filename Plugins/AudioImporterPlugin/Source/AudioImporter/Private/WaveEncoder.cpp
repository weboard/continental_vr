// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#include "WaveEncoder.h"

WaveEncoder::WaveEncoder(FString filename, FString rawFile, RawMetadata metadata) : Encoder(filename, rawFile, metadata)
{
	m_AudioCodec = nullptr;
	m_AudioCodecContext = nullptr;
	m_FormatContext = nullptr;
	m_OutputFormat = nullptr;
	m_SoundData = nullptr;
}

void list_supported_fmt(const AVCodec *codec)
{
	UE_LOG(LogAudioImporter, Log, TEXT("Supported formats"));
	const enum AVSampleFormat *p = codec->sample_fmts;

	while (*p != AV_SAMPLE_FMT_NONE)
	{
		FString myString(av_get_sample_fmt_name(*p));
		UE_LOG(LogAudioImporter, Log, TEXT("Format %s"), *myString);
		p++;
	}
}

bool WaveEncoder::InitFormat(const char* audioFileName)
{
	int result = avformat_alloc_output_context2(&m_FormatContext, nullptr, nullptr, audioFileName);
	if (result < 0) return false;

	m_OutputFormat = m_FormatContext->oformat;

	AVStream *audioStream = avformat_new_stream(m_FormatContext, m_AudioCodec);
	if (audioStream == nullptr)
	{
		avformat_free_context(m_FormatContext);
		return 0;
	}

	audioStream->id = m_FormatContext->nb_streams - 1;
	//audioStream->time_base = { 1, 44100 };
	audioStream->time_base = { 1, _metadata.sampleRate };
	result = avcodec_parameters_from_context(audioStream->codecpar, m_AudioCodecContext);
	if (result < 0)
	{
		avformat_free_context(m_FormatContext);
		UE_LOG(LogAudioImporter, Log, TEXT("Wave encoding error : Init Format - parameters from context error"));
		return false;
	}

	av_dump_format(m_FormatContext, 0, audioFileName, 1);
	if (!(m_OutputFormat->flags & AVFMT_NOFILE))
	{
		result = avio_open(&m_FormatContext->pb, audioFileName, AVIO_FLAG_WRITE);
		if (result < 0)
		{
			avformat_free_context(m_FormatContext);
			UE_LOG(LogAudioImporter, Log, TEXT("Wave encoding error : Init Format - avio_open error"));
			return false;
		}
	}

	return true;
}

int WaveEncoder::ReadRawFile()
{

	FString inputfile_name = _rawFile;// GetRawFilePath(_rawFile);
	const char* filename = TCHAR_TO_ANSI(*inputfile_name);

	/*std::string file_name = TCHAR_TO_UTF8(*FPaths::GameContentDir());
	file_name.append("lofi.raw");
	filename = file_name.c_str();*/

	std::ifstream input(filename, std::ios::binary);
	buffer = std::vector<unsigned char>(std::istreambuf_iterator<char>(input), {});

	
	int size = buffer.size();
	
	m_SoundData = reinterpret_cast<char*>(buffer.data());
	//UE_LOG(LogAudioImporter,Log, TEXT("OPEN RAW FILE %s WITH SIZE OF %i"), *inputfile_name, size);
	sizeRaw = size;
	return size;
}


WaveEncoder::~WaveEncoder()
{
}

void WaveEncoder::Encode()
{

	FString outfile_name = GetFilePath(_filename);

	const char *wavfile;

	wavfile = TCHAR_TO_ANSI(*outfile_name);


	/*const char* filename;
	std::string file_name = TCHAR_TO_UTF8(*FPaths::GameContentDir());
	file_name.append("lofi.wav");
	filename = file_name.c_str();*/



	//av_register_all();
	//avcodec_register_all();

	AVCodecID codecID = AV_CODEC_ID_PCM_S16LE;// AV_CODEC_ID_PCM_S16LE;// ;RIGHT AV_CODEC_ID_PCM_F32LE
	m_AudioCodec = avcodec_find_encoder(codecID);
	if (m_AudioCodec == nullptr)
	{
		throw "Audio codec is null";
		return;
	}
	list_supported_fmt(m_AudioCodec);


	m_AudioCodecContext = avcodec_alloc_context3(m_AudioCodec);
	if (m_AudioCodecContext == nullptr)
	{
		throw "Audio codec context is null";
		return;
	}

	//Setting parameters
	
	FString strfrm = FString(_metadata.format);
	
	UE_LOG(LogAudioImporter, Log, TEXT("Wave encoding : Wav metadata : %i , frm : %s, chan : %i"), _metadata.sampleRate, *strfrm, _metadata.nChannels);

	//m_AudioCodecContext->bit_rate = 64000;
	m_AudioCodecContext->sample_rate = _metadata.sampleRate;// 44100;
	
	m_AudioCodecContext->sample_fmt = AV_SAMPLE_FMT_S16;// AV_SAMPLE_FMT_FLT,//AV_SAMPLE_FMT_FLT;// AV_SAMPLE_FMT_S16;//Special WAV (MP3 -> AV_SAMPLE_FMT_S16P par ex)
	m_AudioCodecContext->channels = _metadata.nChannels;
	m_AudioCodecContext->channel_layout = av_get_default_channel_layout(_metadata.nChannels);
	m_AudioCodecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
	
	//Turn on encoder
	int result = avcodec_open2(m_AudioCodecContext, m_AudioCodec, NULL);
	if (result < 0)
	{
		throw "avcodec_open2 error";
		return;
	}

	if (!InitFormat(wavfile))
	{
		throw "init format error";
		return;
	}

	result = avformat_write_header(m_FormatContext, NULL);
	if (result < 0)
	{
		throw "avformat_write_header error";
		return;
	}

	AVFrame *frame = av_frame_alloc();
	if (frame == nullptr)
	{
		throw "av_frame_alloc error";
		return;
	}

	int nb_samples = 0;
	if (m_AudioCodecContext->codec->capabilities & AV_CODEC_CAP_VARIABLE_FRAME_SIZE)
		nb_samples = 32;// 10000; 10000 added regular glitches on output wav file
	else {
		UE_LOG(LogAudioImporter, Log, TEXT("AudioCodecContext does not have the AV_CODEC_CAP_VARIABLE_FRAME_SIZE capability"));
		nb_samples = m_AudioCodecContext->frame_size;
	}

	//Set the parameters of the frame
	frame->nb_samples = nb_samples;
	frame->format = m_AudioCodecContext->sample_fmt;
	frame->channel_layout = m_AudioCodecContext->channel_layout;

	//Apply for data memory
	result = av_frame_get_buffer(frame, 0);
	if (result < 0)
	{
		av_frame_free(&frame);
		throw "av_frame_get_buffer error";
		return;
	}

	//Set the frame to be writable
	result = av_frame_make_writable(frame);
	if (result < 0)
	{
		throw "av_frame_make_writable error";
		return;
	}

	int pcmDataSize;
	
	pcmDataSize = ReadRawFile();

	int perFrameDataSize = frame->linesize[0];
	int count = pcmDataSize / perFrameDataSize;
	bool needAddOne = false;
	if (pcmDataSize % perFrameDataSize != 0)
	{
		count++;
		needAddOne = true;
		UE_LOG(LogAudioImporter, Log, TEXT("perFrameDataSize is not a multiple of pcmDataSize, a frame will be added"));
	}

	int frameCount = 0;

	unsigned char* bbuf = m_FormatContext->pb->buffer;
	char* bufchar = reinterpret_cast<char *>(bbuf);
	FString strfrmt = FString(bufchar);
	UE_LOG(LogAudioImporter, Log, TEXT("Wave encoding : format output : %s"), *strfrmt);

	for (int i = 0; i < count; ++i)
	{


		AVPacket *pkt = av_packet_alloc();
		if (pkt == nullptr)
		{
			throw "can't init packet";
			return;
		}
		av_init_packet(pkt);
		if (i == count - 1)
			perFrameDataSize = pcmDataSize % perFrameDataSize;

		//WAV
		memset(frame->data[0], 0, perFrameDataSize);
		//frame->data[0] = &data;
		//memcpy(frame->data[0], &(soundData[perFrameDataSize*i]), perFrameDataSize);
		//memcpy(frame->data[0], &(data), perFrameDataSize);
		memcpy(frame->data[0], &(m_SoundData[perFrameDataSize * i]), perFrameDataSize);


		frame->pts = frameCount++;
		result = avcodec_send_frame(m_AudioCodecContext, frame);

		result = avcodec_receive_packet(m_AudioCodecContext, pkt);
		if (result < 0)
		{
			av_packet_free(&pkt);
		}
		/*else {
			UE_LOG(LogAudioImporter, Log, TEXT("result is smaller than 0 : %i"), result);
		}*/
		av_packet_rescale_ts(pkt, m_AudioCodecContext->time_base, m_FormatContext->streams[0]->time_base);
		pkt->stream_index = 0;
		result = av_interleaved_write_frame(m_FormatContext, pkt);
		av_packet_free(&pkt);
	}

	av_write_trailer(m_FormatContext);
	avio_closep(&m_FormatContext->pb);
	av_frame_free(&frame);
	avcodec_free_context(&m_AudioCodecContext);
	if (m_FormatContext != nullptr)
		avformat_free_context(m_FormatContext);
}