// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#include "Decoder.h"
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>



Decoder::Decoder(FString filename, FString rawPath) : _filename(filename), _rawPath(rawPath)
{

}


FString Decoder::GetFilePath()
{

	FString res = FString(_filename);

	std::string stdpathString = TCHAR_TO_UTF8(*_filename);
	std::experimental::filesystem::path pathToTest(stdpathString);
	if (pathToTest.is_relative ()) {
		std::string inputfilename = TCHAR_TO_UTF8(*FPaths::ProjectContentDir());
		inputfilename.append(TCHAR_TO_UTF8(*_filename));
		res = FString(inputfilename.c_str());
	}

	return res;
}