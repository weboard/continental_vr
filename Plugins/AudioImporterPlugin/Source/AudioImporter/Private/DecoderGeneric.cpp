// Copyright 2019 Isara Technologies SAS. All Rights Reserved.


#include "DecoderGeneric.h"


#define MAX_AUDIO_FRAME_SIZE 192000 //1 second of 48khz 32bit audio

DecoderGeneric::DecoderGeneric(FString filename, FString rawPath) : Decoder(filename, rawPath)
{
}

DecoderGeneric::~DecoderGeneric()
{
}

RawMetadata DecoderGeneric::Decode()
{
	RawMetadata rm = RawMetadata();

	AVFormatContext *pFormatCtx;
	int audioStream;
	AVCodecContext *pCodecCtx;
	AVCodec *pCodec;
	AVPacket *packet;
	uint8_t *out_buffer;
	AVFrame *pFrame;

	int ret;
	uint32_t len = 0;
	int got_picture;
	int index = 0;
	int64_t in_channel_layout;
	struct SwrContext* au_convert_ctx;

	FString path = GetFilePath();
	std::string pathStr = std::string(TCHAR_TO_UTF8(*path));
	FString outputpath = _rawPath;// GetFilePath();
	//outputpath.Append(".raw");
	std::string outputPathStr = std::string(TCHAR_TO_UTF8(*outputpath));
	//FILE *pFile = fopen(outputPathStr.c_str(), "wb");
	FILE *pFile;
	errno_t err = fopen_s(&pFile, outputPathStr.c_str(), "wb");
	if (err != 0) {
		char buff[256];
		strerror_s(buff, 100, err);
		char* buffChar = buff;
		FString errorString = UTF8_TO_TCHAR(buffChar);
		UE_LOG(LogAudioImporter, Log, TEXT("Error during opening output file with error : %s"), *errorString);
		throw "Error during opening ouptut file";
		return rm;
	}
	//av_register_all();
	avformat_network_init();
	pFormatCtx = avformat_alloc_context();
	//Open
	if (avformat_open_input(&pFormatCtx, pathStr.c_str(), NULL, NULL) != 0)
	{
		throw "Could not open input stream";
		return rm;
	}

	//Retrieve stream information
	if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		throw "Couldn't find stream information";
		return rm;
	}

	//Dump valid information onto standard error
	av_dump_format(pFormatCtx, 0, pathStr.c_str(), false);

	//Find the first audio stream
	audioStream = -1;
	for (unsigned int i = 0; i < pFormatCtx->nb_streams; i++)
	{
		if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
		{
			audioStream = i;
			break;
		}
	}
	if (audioStream == -1)
	{
		throw "Didn't find an audio stream";
		return rm;
	}

	//Get a pointer to the codec context for the audio stream
	pCodecCtx = pFormatCtx->streams[audioStream]->codec;

	//Find the decoder for the audio stream
	pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
	if (pCodec == NULL)
	{
		throw "Codec not found";
		return rm;
	}

	//Open codec
	if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
	{
		throw "Could not open codec";
		return rm;
	}

	packet = (AVPacket*)av_malloc(sizeof(AVPacket));
	av_init_packet(packet);

	//Out audio param
	uint64_t out_channel_layout = AV_CH_LAYOUT_STEREO;
	//if (av_get_channel_layout_nb_channels(pCodecCtx->channel_layout) > 2) {
	//	out_channel_layout = pCodecCtx->channel_layout;
	//}

	

	if (pCodecCtx->channels == 1) 
		out_channel_layout = AV_CH_LAYOUT_MONO;

	UAudioImporterSettings* Settings = GetMutableDefault<UAudioImporterSettings>();
	if (Settings->bConvertMonoAudioFilesToStereo)
	{
		out_channel_layout = AV_CH_LAYOUT_STEREO;
	}

	//uint64_t out_channel_layout = AV_CH_LAYOUT_MONO;// AV_CH_LAYOUT_STEREO;
	int out_nb_samples = pCodecCtx->frame_size;

	if (out_nb_samples == 0)
		out_nb_samples = 4096*2;// 2048;

	AVSampleFormat out_sample_fmt = AV_SAMPLE_FMT_S16;// AV_SAMPLE_FMT_S16AV_SAMPLE_FMT_FLT;
	int out_sample_rate = pCodecCtx->sample_rate;// 11025;// 44100;
	int out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
	
	//Out buffer size
	int out_buffer_size = av_samples_get_buffer_size(NULL, out_channels, out_nb_samples, out_sample_fmt, 1);
	out_buffer = (uint8_t*)av_malloc(MAX_AUDIO_FRAME_SIZE * 2);
	pFrame = av_frame_alloc();

	rm.sampleFormat = out_sample_fmt;// AV_SAMPLE_FMT_S16;
	rm.nChannels = pCodecCtx->channels;// 1;// 2;
	rm.sampleRate = out_sample_rate;
	rm.outfileName = outputPathStr.c_str();

	if (rm.nChannels > 2) {
		UE_LOG(LogAudioImporter, Log, TEXT("Input file has more than 2 channels (%i)"), pCodecCtx->channels);
		throw "Input file has more than 2 channels";
		return rm;
	}

	if (Settings->bConvertMonoAudioFilesToStereo)
	{
		rm.nChannels = 2;
	}

	in_channel_layout = av_get_default_channel_layout(pCodecCtx->channels);
	au_convert_ctx = swr_alloc();
	au_convert_ctx = swr_alloc_set_opts(au_convert_ctx, out_channel_layout, out_sample_fmt, out_sample_rate, in_channel_layout, pCodecCtx->sample_fmt, pCodecCtx->sample_rate, 0, NULL);
	int res = swr_init(au_convert_ctx);

	while (av_read_frame(pFormatCtx, packet) >= 0)
	{
		if (packet->stream_index == audioStream)
		{
			/*ret = avcodec_receive_frame(pCodecCtx, pFrame);
			if (ret < 0)
			{
				throw "Error during receving frame";
			}
			ret = avcodec_send_packet(pCodecCtx, packet);
			if (ret < 0)
			{
				throw "Error during sending packet";
			}*/
			ret = avcodec_decode_audio4(pCodecCtx, pFrame, &got_picture, packet);
			
			//Frame size of wma files is variable, so the out buffer size must be calculated for each frames
			//This computation is made with MP3 and AAC also (although the frame size is supposed to be invariant for these formats) to avoid glitch sound at the beginning
			//if (pCodecCtx->codec_id == AV_CODEC_ID_WMAV2 || pCodecCtx->codec_id == AV_CODEC_ID_WMAV1)
			//{
				int actualframesize = pFrame->nb_samples;
				out_nb_samples = actualframesize;
				out_buffer_size = av_samples_get_buffer_size(NULL, out_channels, out_nb_samples, out_sample_fmt, 1);
			//}

			if (ret < 0)
			{
				throw "Error in decoding frame";
				return rm;
			}
			
			//got_picture = 1;
			if (got_picture > 0)
			{
				int resSWC = swr_convert(au_convert_ctx, &out_buffer, MAX_AUDIO_FRAME_SIZE, (const uint8_t**)pFrame->data, pFrame->nb_samples);
				fwrite(out_buffer, 1, out_buffer_size, pFile);
				//fwrite(pFrame->data, 1, pFrame->nb_samples, pFile);
				index++;
			}
			else {
				UE_LOG(LogAudioImporter, Log, TEXT("got_picture is greater than 0 : %i"), got_picture);
			}
		}
		else {
			UE_LOG(LogAudioImporter, Log, TEXT("packet->stream_index is not audioStream : stream_index = %i and audioStream = %i "), packet->stream_index, audioStream);
		}
		av_free_packet(packet);
	}

	swr_free(&au_convert_ctx);
	fclose(pFile);
	av_free(out_buffer);
	avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);

	return rm;
}