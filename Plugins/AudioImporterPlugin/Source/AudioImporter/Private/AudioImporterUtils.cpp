// Copyright 2019 Isara Technologies SAS. All Rights Reserved.


#include "AudioImporterUtils.h"
//#include "EditorFramework/AssetImportData.h"
#include "Engine/Classes/EditorFramework/AssetImportData.h"
#include "Interfaces/IAudioFormat.h"
#include "VorbisAudioInfo.h"

AudioImporterUtils::AudioImporterUtils()
{
}

AudioImporterUtils::~AudioImporterUtils()
{
}

/*USoundWave* CreateSoundWaveFromRawWav(TArray<uint8> Bytes, UFactory factory ) {
	GEditor->GetEditorSubsystem<UImportSubsystem>()->BroadcastAssetPreImport (this, Class, InParent, Name, FileType);
}*/

void AudioImporterUtils::GetSoundWaveFromRawWav(TArray<uint8> Bytes, USoundWave* sw, FString sourceFileName)
{


	FWaveModInfo WaveInfo;
	if (WaveInfo.ReadWaveInfo(Bytes.GetData(), Bytes.Num()))
	{
		USoundWave* SoundWave = sw;

		SoundWave->RawData.Lock(LOCK_READ_WRITE);
		void* LockedData = SoundWave->RawData.Realloc(Bytes.Num());
		FMemory::Memcpy(LockedData, Bytes.GetData(), Bytes.Num());
		SoundWave->RawData.Unlock();

		SoundWave->SetSampleRate(*WaveInfo.pSamplesPerSec);
		SoundWave->NumChannels = *WaveInfo.pChannels;

		/*const int32 BytesDataPerSecond = *WaveInfo.pChannels * (*WaveInfo.pBitsPerSample / 8.f) * *WaveInfo.pSamplesPerSec;
		if (BytesDataPerSecond)
		{
			SoundWave->Duration = WaveInfo.SampleDataSize / BytesDataPerSecond;
		}*/

		const int32 BytesDataPerSecond = *WaveInfo.pChannels * *WaveInfo.pBitsPerSample * *WaveInfo.pSamplesPerSec;
		if (BytesDataPerSecond)
		{
			SoundWave->Duration = *WaveInfo.pWaveDataSize * 8.0f / BytesDataPerSecond;
		}

		SoundWave->RawPCMDataSize = WaveInfo.SampleDataSize;

		SoundWave->RawPCMData = static_cast<uint8*>(FMemory::Malloc(WaveInfo.SampleDataSize));
		FMemory::Memcpy(SoundWave->RawPCMData, WaveInfo.SampleDataStart, WaveInfo.SampleDataSize);
	}
	else
	{
		UE_LOG(LogAudioImporter, Log, TEXT("Error creating SoundWave from Raw file!"));
	}
}