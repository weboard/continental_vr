// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#include "Encoder.h"
#include "RawMetadata.h"
#include "AudioImporterUtils.h"
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>


Encoder::Encoder(FString filename, FString rawFile, RawMetadata metadata)
{
	_filename = filename;
	_rawFile = rawFile;
	_metadata = metadata;
}

FString Encoder::GetFilePath(FString file)
{
	FString res = FString(file);

	std::string stdpathString = TCHAR_TO_UTF8(*file);
	std::experimental::filesystem::path pathToTest(stdpathString);
	if (pathToTest.is_relative()) {

		std::string inputfilename = TCHAR_TO_UTF8(*FPaths::ProjectContentDir());
		inputfilename.append(TCHAR_TO_UTF8(*file));
		res = FString(inputfilename.c_str());

	}

	return res;
}

FString Encoder::GetRawFilePath(FString file)
{
	//FString res = FString(AudioImporterUtils::GetSystemTempDirectory() + "/" + file);
	
	

	return file;
}