// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#include "AudioImporterRuntime.h"

#include "WaveEncoder.h"
#include "RawMetadata.h"
#include "AudioImporterUtils.h"

#include "Sound/SoundWave.h" 
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>

#if WITH_EDITOR
USoundWave* UAudioImporterRuntime::saved = NewObject<USoundWave>();
#else
USoundWave* UAudioImporterRuntime::saved = nullptr;
#endif


USoundWave* UAudioImporterRuntime::ConvertAudioFile(UObject* WorldContextObject, FString path, TArray<uint8>& RawData)
{
	FString inputPath = path;
	
	std::string stdpathString = TCHAR_TO_UTF8(*inputPath);
	std::experimental::filesystem::path pathToTest(stdpathString); // Construct the path from a string.
	if (pathToTest.is_relative()) {
		FString withInputPath = path;
		FString FullPath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*withInputPath);
		inputPath = FullPath;
	}
	FString outfile = FString(inputPath + ".wav");
	FString rawFile = FString(AudioImporterUtils::GetSystemTempDirectory() + "temp.raw");


	//Conversion procedure
	TSharedPtr<Decoder, ESPMode::ThreadSafe> decoder;
	decoder = TSharedPtr<Decoder, ESPMode::ThreadSafe>(new DecoderGeneric(inputPath, rawFile));

	RawMetadata metadata{ "",0,0,"",AVSampleFormat() };
	try {
		metadata = decoder->Decode();
	}
	catch (const char* ex)
	{
		FString exStr = FString(ex);
		UE_LOG(LogAudioImporter, Error, TEXT("Error while decoding input audio : %s"), *exStr);
	}

	FString fmtStr = FString(metadata.format);
	FString outfileNameStr = FString(metadata.outfileName);
	int nchannels = metadata.nChannels;
	int sampleRate = metadata.sampleRate;
	TUniquePtr<WaveEncoder> encoder(new WaveEncoder(outfile, rawFile, metadata));

	try
	{
		encoder->Encode();
	}
	catch (const char* ex)
	{
		FString exStr = FString(ex);
		UE_LOG(LogAudioImporter, Error, TEXT("Error while encoding WAVE : %s"), *exStr);
	}
	//End conversion

	std::string inputfilename = TCHAR_TO_UTF8(*outfile);

	std::ifstream input(inputfilename.c_str(), std::ios::binary);
	std::vector<unsigned char> buffer = std::vector<unsigned char>(std::istreambuf_iterator<char>(input), {});
	TArray<uint8> bufferarray = TArray<uint8>();
	for (size_t i = 0; i < buffer.size(); i++)
		bufferarray.Add(buffer[i]);

	RawData = bufferarray;
	
	USoundWave* testWave = NewObject<USoundWave>(USoundWave::StaticClass());

	//testWave->RawPCMData
	
	AudioImporterUtils utils;
	utils.GetSoundWaveFromRawWav(bufferarray, testWave, path);
	input.close();

	
	//Remove raw file
	std::string rawFilestdStr = TCHAR_TO_UTF8(*rawFile);
	if (remove(rawFilestdStr.c_str()) != 0)
	{
//		UE_LOG(LogAudioImporter, Log, TEXT("Error during deleting raw file"));
	}

	std::string waveFilestdStr = TCHAR_TO_UTF8(*IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*FPaths::ProjectContentDir()));
	waveFilestdStr.append(TCHAR_TO_UTF8(*outfile));

	//Remove wave file
	FString waveFile = FPaths::ProjectContentDir().Append(outfile);
	FString waveAbsolutePath = outfile;// IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*waveFile);
	if (!FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*waveAbsolutePath))
	{
//		UE_LOG(LogAudioImporter, Log, TEXT("Could Not Find File"));
	}
	
	return testWave;
}