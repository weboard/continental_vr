// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#include "AudioImporterModule.h"

AudioImporterModule::AudioImporterModule()
	: Initialized(false) {
	AVDeviceLibrary = nullptr;
	AVFilterLibrary = nullptr;
	PostProcLibrary = nullptr;
	SWScaleLibrary = nullptr;
	AVFormatLibrary = nullptr;
	AVCodecLibrary = nullptr;
	SWResampleLibrary = nullptr;
	AVUtilLibrary = nullptr;
}

void AudioImporterModule::StartupModule()
{

	AVUtilLibrary = LoadLibrary(TEXT("avutil"), TEXT("56"));
	SWResampleLibrary = LoadLibrary(TEXT("swresample"), TEXT("3"));
	AVCodecLibrary = LoadLibrary(TEXT("avcodec"), TEXT("58"));
	AVFormatLibrary = LoadLibrary(TEXT("avformat"), TEXT("58"));
	SWScaleLibrary = LoadLibrary(TEXT("swscale"), TEXT("5"));
	PostProcLibrary = LoadLibrary(TEXT("postproc"), TEXT("55"));
	AVFilterLibrary = LoadLibrary(TEXT("avfilter"), TEXT("7"));
	AVDeviceLibrary = LoadLibrary(TEXT("avdevice"), TEXT("58"));

#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(58, 9, 100)
	//av_register_all();
#endif

	avformat_network_init();
	av_log_set_level(AV_LOG_DEBUG);

	UE_LOG(LogAudioImporter, Display, TEXT("FFmpeg AVCodec version: %d.%d.%d"), LIBAVFORMAT_VERSION_MAJOR, LIBAVFORMAT_VERSION_MINOR, LIBAVFORMAT_VERSION_MICRO);
	UE_LOG(LogAudioImporter, Display, TEXT("FFmpeg license: %s"), UTF8_TO_TCHAR(avformat_license()));

	auto MediaModule = FModuleManager::LoadModulePtr<IMediaModule>("Media");

	if (MediaModule != nullptr)
	{
		//TODO: Implement Capture support
		//MediaModule->RegisterCaptureSupport(*this);
	}
	else {
		UE_LOG(LogAudioImporter, Error, TEXT("Coudn't find the media module"));
	}

	Initialized = true;
}

void AudioImporterModule::ShutdownModule()
{
	if (!Initialized)
	{
		return;
	}

	// unregister capture support
	auto MediaModule = FModuleManager::GetModulePtr<IMediaModule>("Media");

	if (MediaModule != nullptr)
	{
		//MediaModule->UnregisterCaptureSupport(*this);
	}

	if (AVDeviceLibrary) FPlatformProcess::FreeDllHandle(AVDeviceLibrary);
	if (AVFilterLibrary) FPlatformProcess::FreeDllHandle(AVFilterLibrary);
	if (PostProcLibrary) FPlatformProcess::FreeDllHandle(PostProcLibrary);
	if (SWScaleLibrary) FPlatformProcess::FreeDllHandle(SWScaleLibrary);
	if (AVFormatLibrary) FPlatformProcess::FreeDllHandle(AVFormatLibrary);
	if (AVCodecLibrary) FPlatformProcess::FreeDllHandle(AVCodecLibrary);
	if (SWResampleLibrary) FPlatformProcess::FreeDllHandle(SWResampleLibrary);
	if (AVUtilLibrary) FPlatformProcess::FreeDllHandle(AVUtilLibrary);

	Initialized = false;
}

void* AudioImporterModule::LoadLibrary(const  FString& name, const FString& version) {

	FString BaseDir = IPluginManager::Get().FindPlugin("AudioImporter")->GetBaseDir();

	FString LibDir;
	FString extension;
	FString prefix;
	FString separator;
	extension = TEXT(".dll");
	prefix = "";
	separator = "-";

	LibDir = FPaths::Combine(*BaseDir, TEXT("Source/ThirdParty/ffmpeg/bin/vs/x64"));
	if (!LibDir.IsEmpty()) {
		FString LibraryPath = FPaths::Combine(*LibDir, prefix + name + separator + version + extension);
		return FPlatformProcess::GetDllHandle(*LibraryPath);
	}
	return nullptr;
}

IMPLEMENT_MODULE(AudioImporterModule, AudioImporter);