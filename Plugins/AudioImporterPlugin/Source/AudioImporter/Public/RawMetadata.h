// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once
#include "libavformat/avformat.h"

/**
 * Describe some useful properties of metadata for an audio file
 */
struct RawMetadata
{
	const char* format;
	int nChannels;
	int sampleRate;
	const char* outfileName;
	AVSampleFormat sampleFormat;
};