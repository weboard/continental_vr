// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "Core.h"

#include "AudioImporterUtils.h"
#include "string"
#include "RawMetadata.h"

/**
 * Allow a class to decode an audio file to raw data
 */
class AUDIOIMPORTER_API Decoder
{
public:
	/**
	* Create an audio decoder
	* @param filename input audio file path
	*/
	Decoder(FString filename, FString rawPath);
	//~IDecoder();
	virtual RawMetadata Decode() = 0;

	virtual ~Decoder() {};
protected:

	FString _filename;
	FString _rawPath;

	/**
	 * Give the path of the input file
	 * @return path of the input file
	 */
	FString GetFilePath();


};
