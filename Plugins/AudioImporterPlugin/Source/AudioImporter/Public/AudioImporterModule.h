// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "AudioImporterPrivate.h"

#include "IMediaCaptureSupport.h"
#include "Modules/ModuleManager.h"

#include "Core.h"
#include "Interfaces/IPluginManager.h"

#include "IMediaModule.h"
#include "Modules/ModuleInterface.h"
#include "Templates/SharedPointer.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <iostream>

#include <fstream>
#include <iterator>
#include <vector>

extern  "C" {
#include "libavutil/mem.h"
#include "libavutil/frame.h"
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libavutil/mathematics.h"
#include "libavutil/opt.h"
#include "libavutil/channel_layout.h"
#include "libavutil/imgutils.h"
#include "libavutil/common.h"
#include "libavutil/samplefmt.h"

#include "libswresample/swresample.h"
}

#include "Sound/SoundWave.h"
#include "DecoderGeneric.h"
#include "WaveEncoder.h"

#define INBUF_SIZE 4096
#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096

DEFINE_LOG_CATEGORY(LogAudioImporter);
#define LOCTEXT_NAMESPACE "AudioImporterSettings"

/**
* 
*/
class AudioImporterModule : public FDefaultGameModuleImpl
{
public:
	AudioImporterModule();
	/**
	* First method called to initialize plugin
	*/
	virtual void StartupModule() override;
	/**
	* Called to clean up plugin
	*/
	virtual void ShutdownModule() override;
	
	virtual bool SupportsDynamicReloading() override
	{
		return true;
	}


protected:
	/**
	* Load static library
	*/
	void* LoadLibrary(const FString& name, const FString& version);

private:
	void* AVUtilLibrary;
	void* SWResampleLibrary;
	void* AVCodecLibrary;
	void* SWScaleLibrary;
	void* AVFormatLibrary;
	void* PostProcLibrary;
	void* AVFilterLibrary;
	void* AVDeviceLibrary;

	/** Whether the module has been initialized. */
	bool Initialized;
};