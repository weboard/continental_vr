// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AudioImporterSettings.generated.h"

/**
 * Contains properties to expose to Editor Settings window
 */
UCLASS(config = Game, defaultconfig)
class AUDIOIMPORTER_API UAudioImporterSettings : public UObject
{
	GENERATED_BODY()
	
public:
	UAudioImporterSettings(const FObjectInitializer& ObjectInitializer);

	/**
	* True to convert mono files to stereo files during conversion, false otherwise
	*/
	UPROPERTY(EditAnywhere, config, Category = Custom)
	bool bConvertMonoAudioFilesToStereo;
};
