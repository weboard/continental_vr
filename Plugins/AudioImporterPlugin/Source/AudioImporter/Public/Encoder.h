// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "Core.h"

#include "string"

/**
 * Allow a class to encode a raw file to an audio file
 */
class AUDIOIMPORTER_API Encoder
{
public:
	/**
	* Set up an encoder
	* @param filename destination filepath
	* @param rawFile input raw audio file
	* @param metadata describing raw audio file
	*/
	Encoder(FString filename, FString rawFile, RawMetadata metadata);
	virtual ~Encoder() {};

	virtual void Encode() = 0;

protected:
	FString _filename;
	RawMetadata _metadata;
	FString _rawFile;

	/**
	 * Get the entire filepath for a file name
	 * @param file filenane
	 * @return path of a file
	 */
	FString GetFilePath(FString file);

	/**
	* Get the entire filepath for a raw file (in system temp directory)
	* @param file filename
	* @return absolute path of file
	*/
	FString GetRawFilePath(FString file);
	
};
