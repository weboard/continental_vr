// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Sound/SoundWave.h"
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>
#undef _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
/**
 * Contain generics functions
 */
class AUDIOIMPORTER_API AudioImporterUtils
{
public:
	AudioImporterUtils();
	~AudioImporterUtils();
	/**
	* Convert wave data to an USoundWave
	* @param Bytes raw data
	* @param sw USoundWave pointer to create
	*/
	void GetSoundWaveFromRawWav(TArray<uint8> Bytes, USoundWave* sw, FString sourceFileName);

	static FString GetSystemTempDirectory() {
		std::string res_string = std::experimental::filesystem::temp_directory_path().string();
		FString res(res_string.c_str());
		if (res_string.empty()) {
			res = FPaths::ConvertRelativePathToFull(FPaths::ProjectDir());
		}
		return res;
		
	}
};
