// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Encoder.h"
#include "RawMetadata.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <iostream>

#include <fstream>
#include <iterator>
#include <vector>

extern  "C" {
#include "libavutil/mem.h"
#include "libavutil/frame.h"
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libavutil/mathematics.h"
#include "libavutil/opt.h"
#include "libavutil/channel_layout.h"
#include "libavutil/imgutils.h"
#include "libavutil/common.h"
#include "libavutil/samplefmt.h"

#include "libswresample/swresample.h"
}


/**
 * Allows the encoding of a raw audio file in WAV audio file format
 */
class AUDIOIMPORTER_API WaveEncoder : public Encoder
{
public:
	/**
	 * Init a wav encoder
	 * @param filename output wav to be created
	 * @param rawFile input raw audio file to be encoded
	 * @param metadata metadata of raw audio file
	 */
	WaveEncoder(FString filename, FString rawFile, RawMetadata metadata);
	virtual ~WaveEncoder();

	
	virtual void Encode() override;

private:

	/**
	 * Read the raw audio file
	 */
	int ReadRawFile();
	/**
	 * Init output format
	 * @param audioFileName output file
	 */
	bool InitFormat(const char* audioFileName);

	AVCodec *m_AudioCodec = nullptr;
	AVCodecContext *m_AudioCodecContext = nullptr;
	AVFormatContext *m_FormatContext = nullptr;
	AVOutputFormat *m_OutputFormat = nullptr;

	char* m_SoundData = nullptr;
	std::vector<unsigned char> buffer;
	int sizeRaw;

};