// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

extern  "C" {
#include "libswresample/swresample.h"
}

#include "AudioImporterSettings.h"
#include "Decoder.h"
/**
 * 
 */
class AUDIOIMPORTER_API DecoderGeneric : public Decoder
{
public:
	/**
	 * Init an AAC decoder
	 * @param filename name of the input audio file
	*/
	DecoderGeneric(FString filename, FString rawPath);
	virtual ~DecoderGeneric();
	
	/**
	 * Decode the file and create a raw file
	 * @return Metadata struct with information of input audio file
	*/
	virtual RawMetadata Decode();
};
