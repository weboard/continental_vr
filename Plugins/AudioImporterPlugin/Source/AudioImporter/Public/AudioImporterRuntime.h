// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include <HAL/FileManager.h>
#include "AudioImporterSettings.h"

#include <stdio.h>

#include "DecoderGeneric.h"

#include "Kismet/BlueprintFunctionLibrary.h"

#include "AudioImporterRuntime.generated.h"

/**
 * Contains methods to handle file importation into Unreal Engine
 */
UCLASS()
class AUDIOIMPORTER_API UAudioImporterRuntime : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/**
	* Blueprint node to convert an audio file to an Unreal Engine audio format sound
	* Warning : redondant code with FactoryCreateBinary method when an input file is converted to wave file
	* @param path Absolute or relative path to the sound file to be converted
	* @return pointer to USoundWave created
	*/
	UFUNCTION(BlueprintCallable, Category = "Audio|AudioImporter", meta = (WorldContext = "WorldContextObject"))
		static USoundWave* ConvertAudioFile(UObject* WorldContextObject, FString path, TArray<uint8>& RawData);

private:
	/**
	* The last USoundWave created during runtime is saved here
	* Fear that Unreal Engine garbage collector will destroy the created USoundWave during runtime 
	* Probably work without it, but not tested yet
	*/
	static USoundWave* saved;
	USoundWave* sw;
};
