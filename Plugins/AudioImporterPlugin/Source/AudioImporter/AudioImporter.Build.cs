// Copyright 2019 Isara Technologies SAS. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class AudioImporter : ModuleRules
{

	private string ModulePath
	{
		get { return ModuleDirectory; }
	}

	private string ThirdPartyPath
	{
		get { return Path.GetFullPath(Path.Combine(ModulePath, "../ThirdParty/")); }
	}


	public bool LoadFFmpeg(ReadOnlyTargetRules Target)
	{
		bool isLibrarySupported = false;

		if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
		{
			isLibrarySupported = true;

			string PlatformString = (Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "Win32";
			string LibrariesPath = Path.Combine(Path.Combine(Path.Combine(ThirdPartyPath, "ffmpeg", "lib"), "vs"), PlatformString);


			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "avcodec.lib"));
			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "avdevice.lib"));
			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "avfilter.lib"));
			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "avformat.lib"));
			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "avutil.lib"));
			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "swresample.lib"));
			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "swscale.lib"));

			string[] dlls = {"avcodec-58.dll","avdevice-58.dll", "avfilter-7.dll", "avformat-58.dll", "avutil-56.dll", "swresample-3.dll", "swscale-5.dll", "postproc-55.dll"};

			string BinariesPath = Path.Combine(Path.Combine(Path.Combine(ThirdPartyPath, "ffmpeg", "bin"), "vs"), PlatformString);
			foreach (string dll in dlls)
			{
				PublicDelayLoadDLLs.Add(dll);
				RuntimeDependencies.Add(Path.Combine(BinariesPath, dll), StagedFileType.NonUFS);
			}

		}
		
		if (isLibrarySupported)
		{
			// Include path
			PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "ffmpeg", "include"));
		}


		return isLibrarySupported;
	}

	public AudioImporter(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		OptimizeCode = CodeOptimization.Never;
		bEnableExceptions = true;

		DynamicallyLoadedModuleNames.AddRange(
			new string[] {
				"Media",
			});
        
		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
				"Engine",
				"MediaUtils",
				"RenderCore",
				"Projects",
            });

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"Media",
            });

		//PrivateIncludePaths.AddRange(
		//	new string[] {
		//		"AudioImporter/Private",
		//	});


  //      PublicIncludePaths.AddRange(
  //          new string[] {
  //              "AudioImporter/Public",
  //          });

		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "Public"));
		PrivateIncludePaths.Add(Path.Combine(ModuleDirectory, "Private"));

		LoadFFmpeg(Target);
	}
}
